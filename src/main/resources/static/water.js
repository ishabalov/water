

var spinnerOpts = {
		lines: 10 // The number of lines to draw
		, length: 10 // The length of each line
		, width: 10 // The line thickness
		, radius: 20 // The radius of the inner circle
		, scale: 1 // Scales overall size of the spinner
		, corners: 1 // Corner roundness (0..1)
		, color: '#574200' // #rgb or #rrggbb or array of colors
		, opacity: 0.25 // Opacity of the lines
		, rotate: 0 // The rotation offset
		, direction: 1 // 1: clockwise, -1: counterclockwise
		, speed: 1 // Rounds per second
		, trail: 50 // Afterglow percentage
		, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
		, zIndex: 2e9 // The z-index (defaults to 2000000000)
		, className: 'spinner' // The CSS class to assign to the spinner
		, top: '50%' // Top position relative to parent
		, left: '50%' // Left position relative to parent
		, shadow: false // Whether to render a shadow
		, hwaccel: true // Whether to use hardware acceleration
		, position: 'fixed' // Element positioning	
}

actionUpdate = function ($,action,data,success) {
	var spinnerTarget = $('#root');
	var spinner = new Spinner(spinnerOpts).spin(spinnerTarget[0]);
	var url = "/rest/"+action;
	$.ajax({
		type: "POST",
		url: url,
		data: data,
		success: 
		function(resp,status,xhr){
			if (xhr.status==200) {
				jQuery("#root").html(resp);
				if (success!=null) {
					success($);
				}
			} else {
				spinner.stop();
			}
		},
		error:
		function(resp,status,xhr) {
			spinner.stop();
		}
	});
}

toggleLineManual=function(lineId) {
	var data = "lineId="+lineId;
	actionUpdate(jQuery,'line-toggle',data);
}
toggleEnableLineManual=function(lineId) {
	var data = "lineId="+lineId;
	actionUpdate(jQuery,'line-toggle-enable',data);
}

toggleRunAllLinesManual=function() {
	actionUpdate(jQuery,'run-all-lines-manual');
}
toggleMode=function() {
	actionUpdate(jQuery,'toggle-mode');
}

enable=function(id) {
	var e = document.getElementById(id);
	if (e!=null && e.disabled) {
		e.disabled=false;
	}	
}

disable=function(id) {
	var e = document.getElementById(id);
	if (e!=null && !e.disabled) {
		e.disabled=true;
	}	
}

toggleProgramManual=function(programId) {
	var data = "programId="+programId;
	actionUpdate(jQuery,'toggle-program',data);
}

toggleEnableProgramManual=function(programId) {
	var data = "programId="+programId;
	actionUpdate(jQuery,'toggle-program-enable',data);
}

toggleSchedulerState=function() {
	actionUpdate(jQuery,'toggle-scheduler-state');
}

resetController=function(controllerId) {
	var data = "controllerId="+controllerId;
	actionUpdate(jQuery,'reset-controller',data);
}

toggleSchedulerSuspendResume=function() {
	actionUpdate(jQuery,'toggle-scheduler-state');
}

processUpdate=function(u) {
	switch (u.term) {
	case "innerHtml":
		var e = document.getElementById(u.id);
		if (e!=null) {
			e.innerHTML = u.text;
		}
		break;
	case "inputValue":
		var e = document.getElementById(u.id);
		if (e!=null) {
			e.value = u.text;
		}
		break;
	case "locationHref":
		location.href = u.text;
		break;
	case "evalExpression":
		eval(u.text);
	 	break;
	 case "setClass":
		var e = document.getElementById(u.id);
		if (e!=null) {
			e.className = u.text;
		}
		break;
	}
}

processUpdates=function(r) {
	for (index=0; index < r.length; ++index) {
	    processUpdate(r[index]);
	}
}

update=function($) {
	$.ajax({
		type: "GET",
		url: "/rest/updates",
		success: 
		function(resp,status,xhr){
			if (xhr.status==200) {
				processUpdates(resp);
			}
		}
	});
}

jQuery(document).ready(function($) {
	var timer = $.timer(function() {
		update($);
	});
	timer.set({ time : 500, autostart : true });
});