package org.shabalov.water.config;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import org.jbits.os.Os;
import org.jbits.text.TextHelper;

public class ConfigHelper {

	public static final Integer DEFAULT_PORT=8011;
	public static final String DEFAULT_HOST=Os.HOST_NAME;
	public static final String DEFAULT_ROOT=".";
	public static final Integer DEFAULT_UDP_PORT=18342;
	
	public static final String REMOTE_CONTROLLERS_CONFIG = "remote-controllers.conf";
	public static final String LINES_CONFIG = "lines.conf";
	public static final String PROGRAMS_CONFIG = "programs.conf";
	
	public static final Path getRemoteControllersConfigPath() {
		return ConfigHelper.getConfigRoot().resolve(REMOTE_CONTROLLERS_CONFIG);
	}
	public static final Path getLinesConfigPath() {
		return ConfigHelper.getConfigRoot().resolve(LINES_CONFIG);
	}
	public static final Path getProgramsConfigPath() {
		return ConfigHelper.getConfigRoot().resolve(PROGRAMS_CONFIG);
	}

	public static final String getBindingHostname() {
		return Optional.ofNullable(System.getenv("WATER_HOSTNAME")).orElse(DEFAULT_HOST);
	}
	
	public static final Integer getBindingPort() {
		return Optional.ofNullable(TextHelper.nullSafeToInteger(System.getenv("WATER_PORT"))).orElse(DEFAULT_PORT);	
	}

	public static final Integer getUDPPort() {
		return Optional.ofNullable(TextHelper.nullSafeToInteger(System.getenv("WATER_UDP_PORT"))).orElse(DEFAULT_UDP_PORT);	
	}

	public static final Path getRoot() {
		return Paths.get(Optional.ofNullable(System.getenv("WATER_ROOT")).orElse(DEFAULT_ROOT));
	}
	public static final Path getConfigRoot() {
		return getRoot().resolve("conf");
	}
	public static final Path getDataRoot() {
		return getRoot().resolve("data");
	}

}
