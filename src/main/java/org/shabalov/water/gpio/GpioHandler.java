package org.shabalov.water.gpio;

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jbits.beans.Inject;
import org.jbits.beans.PostConstruct;
import org.jbits.func.UnsafeSupplier;
import org.jbits.log.LogHelper;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.Pin;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import com.pi4j.platform.PlatformManager;
import com.pi4j.system.SystemInfo;

public class GpioHandler {

	private static final Pin[] PINS = {
			// see j8header-*.png
			RaspiPin.GPIO_23,
			RaspiPin.GPIO_07,
			RaspiPin.GPIO_00,
			RaspiPin.GPIO_02,
			RaspiPin.GPIO_03,
			RaspiPin.GPIO_12,
			RaspiPin.GPIO_13,
			RaspiPin.GPIO_14,
			RaspiPin.GPIO_21,
			RaspiPin.GPIO_22
	};
	private static final Pin BLINKER_PIN = RaspiPin.GPIO_25;
	public static final int MAX_GPIO_CHANNELS = PINS.length;

	@Inject protected Logger logger;

	private boolean simulate = false;
	private GpioController gpio=null;
	private final GpioPinDigitalOutput[] gpioChannels = new GpioPinDigitalOutput[MAX_GPIO_CHANNELS];
	private final Thread blinkerThread = new Thread(this::blinkerLoop);
	private final Queue<Integer> blinkerQueue = new LinkedBlockingQueue<>();
	private GpioPinDigitalOutput blinkerGpio;

	private final Runnable shutdownHook=()->{
		LogHelper.log(logger, Level.WARNING, "Shutdown hook activated");
		turnAllOff();
		if (gpio!=null && !simulate) {
			gpio.shutdown();
		}
	};

	@PostConstruct
	protected void init() {
		Runtime.getRuntime().addShutdownHook(new Thread(shutdownHook));
		try {
			gpio =  GpioFactory.getInstance();
			systemInfo();
		} catch (UnsatisfiedLinkError e) {
			LogHelper.log(logger, Level.WARNING, "Native library not loaded, forse simulate");
			simulate=true;
		}		
		if (!simulate) {
			for (int i=0;i<MAX_GPIO_CHANNELS;i++) {
				gpioChannels[i] = gpio.provisionDigitalOutputPin(PINS[i],PinState.LOW);
			}
			blinkerGpio = gpio.provisionDigitalOutputPin(BLINKER_PIN,PinState.LOW);
		}
		blinkerThread.setDaemon(true);
		blinkerThread.start();
		turnAllOff();
		blink(4);
	}

	public void turnAllOff() {
		for (int i=0;i<gpioChannels.length;i++) {
			turnOff(i);
		}
	}

	public void turnOff(int channel) {
		if (simulate) {
			LogHelper.log(logger, Level.INFO, "channel %d off", channel);
		} else {
			GpioPinDigitalOutput g = gpioChannels[channel];
			if (g!=null) {
				g.low();
			} else {
				LogHelper.log(logger, Level.SEVERE, "channel %d is inactive for off", channel);
			}
		}
	}

	public void turnOn(int channel) {
		if (channel>=0 && channel<gpioChannels.length) {
			if (simulate) {
				LogHelper.log(logger, Level.INFO, "channel %d on", channel);
			} else {
				GpioPinDigitalOutput g = gpioChannels[channel];
				if (g!=null) {
					g.high();
				} else {
					LogHelper.log(logger, Level.SEVERE, "channel %d is inactive for on", channel);
				}
			}
		} else {
			LogHelper.log(logger, Level.SEVERE, "gpio channel %d is invalid", channel);
		}
	}

	private void outputIgnoreExceptions(UnsafeSupplier<String> supplier) {
		try {
			LogHelper.log(logger, Level.INFO, "%s", supplier.get());
		} catch (Exception e) {
			// ignore exception
		}
	}

	private void systemInfo() {
		outputIgnoreExceptions(()->String.format("Platform: %s(%s)", PlatformManager.getPlatform().getLabel(),PlatformManager.getPlatform().getId()));
		outputIgnoreExceptions(()->String.format("Serial #: %s", SystemInfo.getSerial()));
		outputIgnoreExceptions(()->String.format("CPU Revision: %s", SystemInfo.getCpuRevision()));
		outputIgnoreExceptions(()->String.format("CPU Architecture: %s", SystemInfo.getCpuArchitecture()));
		outputIgnoreExceptions(()->String.format("CPU Part: %s", SystemInfo.getCpuPart()));
		outputIgnoreExceptions(()->String.format("CPU Temperature: %.2f", SystemInfo.getCpuTemperature()));
		outputIgnoreExceptions(()->String.format("CPU Core Voltage: %.2f", SystemInfo.getCpuVoltage()));
		outputIgnoreExceptions(()->String.format("CPU Model Name: %s", SystemInfo.getModelName()));
		outputIgnoreExceptions(()->String.format("Processor: %s", SystemInfo.getProcessor()));
		outputIgnoreExceptions(()->String.format("Hardware: %s", SystemInfo.getHardware()));
		outputIgnoreExceptions(()->String.format("Hardware Revision: %s", SystemInfo.getRevision()));
		outputIgnoreExceptions(()->String.format("Is Hard Float ABI: %b", SystemInfo.isHardFloatAbi()));
		outputIgnoreExceptions(()->String.format("Board Type: %s", SystemInfo.getBoardType().name()));
		outputIgnoreExceptions(()->String.format("Total Memory: %,d(m)", SystemInfo.getMemoryTotal()/(1024*1024)));
		outputIgnoreExceptions(()->String.format("Used Memory: %,d(m)", SystemInfo.getMemoryUsed()/(1024*1024)));
		outputIgnoreExceptions(()->String.format("OS Name: %s", SystemInfo.getOsName()));
		outputIgnoreExceptions(()->String.format("OS Version: %s", SystemInfo.getOsVersion()));
	}

	private void blinkerLoop() {
		while (true) {
			try {
				Integer count = blinkerQueue.poll();
				if (count!=null) {
					if (simulate) {
						LogHelper.log(logger, Level.INFO, "blink, count=%d", count);
					} else {
						while (count>0) {
//							LogHelper.log(logger, Level.INFO, "pulse, count=%d, pin=%s state=%s", count, blinkerGpio.getPin().getName(),blinkerGpio.getState().getName());
							blinkerGpio.pulse(200,true);
							count--;
							synchronized (blinkerQueue) {
								Thread.sleep(200);
							}
						}
					}
				} else {
					synchronized (blinkerQueue) {
						blinkerQueue.wait(10000);
					}
				}
			} catch (InterruptedException e) {
				// just ignore it
			}
		}
	}

	public void blink(int count) {
		blinkerQueue.add(count);
		synchronized (blinkerQueue) {
			blinkerQueue.notify();
		}
	}

}
