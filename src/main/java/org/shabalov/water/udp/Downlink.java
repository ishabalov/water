package org.shabalov.water.udp;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.DatagramChannel;
import java.util.logging.Logger;

import org.jbits.beans.Inject;
import org.jbits.beans.PostConstruct;

public class Downlink {

	@Inject private Logger logger;
	private DatagramChannel channel;

	@PostConstruct
	public void init() throws IOException {
		channel = DatagramChannel.open();
	}


	public void sendCommand(InetSocketAddress addr, DownlinkMessage message) throws IOException {
		channel.send(message.getBuffer().flip(), addr);
//		LogHelper.log(logger,Level.INFO,"send %d bytes to %s",sent,addr.toString());
	}
}
