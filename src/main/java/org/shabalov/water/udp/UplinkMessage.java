package org.shabalov.water.udp;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class UplinkMessage {
/*
	uint16_t header;
	uint8_t chipid[6];
	uint32_t valves_count;
	uint32_t valves_bits;
    double_t pressure;
    double_t temperature;
    double_t humidity;
	uint32_t upticks;
	uint32_t tick_ms;
	uint32_t stopper;
*/
	
	public final byte[] controllerId;
	public final long uptimeMilliseconds;
	public final int valvesCount;
	public final int valvesBits;
	public final double pressure;
	public final double temperature;
	public final double humidity;

	private UplinkMessage(byte[] controllerId, long uptimeMilliseconds, int valvesCount, int valvesBits, double pressure, double temperature, double humidity) {
		this.controllerId = controllerId;
		this.uptimeMilliseconds = uptimeMilliseconds;
		this.valvesCount = valvesCount;
		this.valvesBits = valvesBits;
		this.pressure = pressure;
		this.temperature = temperature;
		this.humidity = humidity;
	}

	public String getControllerMacAddress() {
        return String.format("%02x:%02x:%02x:%02x:%02x:%02x", controllerId[0],controllerId[1],controllerId[2],controllerId[3],controllerId[4],controllerId[5]);
	}
	
	protected static final UplinkMessage toUplinkMessage(ByteBuffer buffer) throws IOException {
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		short header = buffer.getShort();
		if (header==0) { // header version 0
			byte[] controllerId = new byte[6];
			buffer.get(controllerId);
			int valvesCount = buffer.getInt();
			int valvesBits = buffer.getInt();
			double pressure = buffer.getDouble();
			double temperature = buffer.getDouble();
			double humidity = buffer.getDouble();
			int uptimeMilliseconds = buffer.getInt();
			return new UplinkMessage(controllerId, uptimeMilliseconds, valvesCount, valvesBits, pressure, temperature, humidity);
		} else if (header==1) { // header version 1
				byte[] controllerId = new byte[6];
				buffer.get(controllerId);
				int valvesCount = buffer.getInt();
				int valvesBits = buffer.getInt();
				double pressure = buffer.getDouble();
				double temperature = buffer.getDouble();
				double humidity = buffer.getDouble();
				int upticks = buffer.getInt();
				int tick_ms = buffer.getInt();
				long uptimeMilliseconds = (long) upticks * tick_ms;
				return new UplinkMessage(controllerId, uptimeMilliseconds, valvesCount, valvesBits, pressure, temperature, humidity);
		} else {
			return null;
		}
	}

}
