package org.shabalov.water.udp;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class DownlinkMessage {
	public final DownlinkCommand command;
	public final int param1;
	public final int param2;

	private DownlinkMessage(DownlinkCommand command, int param1, int param2) {
		this.command = command;
		this.param1 = param1;
		this.param2 = param2;
	}

	private static final DownlinkMessage PING = new DownlinkMessage(DownlinkCommand.ping, 0, 0);
	private static final DownlinkMessage RESET = new DownlinkMessage(DownlinkCommand.reset, 0, 0);
	
	public static final DownlinkMessage ping() {
		return PING;
	}
	public static final DownlinkMessage toggle(int valve, int interval) {
		return new DownlinkMessage(DownlinkCommand.toggle,valve,interval);
	}
	public static final DownlinkMessage off(int valve) {
		return new DownlinkMessage(DownlinkCommand.off,valve,0);
	}
	public static final DownlinkMessage reset() {
		return RESET;
	}
	
//	typedef struct {
//		uint8_t header1;
//		uint8_t header2;
//		uint16_t command_code;
//		uint32_t parameter1;
//		uint32_t parameter2;
//	} binary_command;
//
//	#define COMMAND_PREFIX_1 0
//	#define COMMAND_PREFIX_2 37
//
//	#define COMMAND_PING   0
//	#define COMMAND_TOGGLE 1
//	#define COMMAND_OFF    2
	
	private static final byte PREFIX1 = 0;
	private static final byte PREFIX2 = 37;
	
	protected ByteBuffer getBuffer() {
		ByteBuffer buffer = ByteBuffer.allocate(32);
		buffer.order(ByteOrder.LITTLE_ENDIAN);
		buffer.put(PREFIX1);
		buffer.put(PREFIX2);
		buffer.putShort((short) command.ordinal());
		buffer.putInt(param1);
		buffer.putInt(param2);
		return buffer;
	}
}
