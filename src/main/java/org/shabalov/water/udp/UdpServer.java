package org.shabalov.water.udp;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jbits.beans.Inject;
import org.jbits.beans.PostConstruct;
import org.jbits.log.LogHelper;
import org.shabalov.water.config.ConfigHelper;
import org.shabalov.water.scheduler.Scheduler;

public class UdpServer implements Runnable {

	private static final int RECEIVE_BUFFER_SIZE = 1024;

	private final Thread udpServerThread;  

	@Inject private Logger logger;
	@Inject private Scheduler scheduler;

	public UdpServer() {
		udpServerThread = new Thread(this,"UDP Server Thread");
		udpServerThread.setDaemon(true);
	}

	@PostConstruct
	public void init() throws SocketException {
		udpServerThread.start();
	}

	@Override
	public void run() {
		try {
			Selector selector = Selector.open();
			DatagramChannel channel = DatagramChannel.open();
			InetSocketAddress isa = new InetSocketAddress(InetAddress.getByName(ConfigHelper.getBindingHostname()),ConfigHelper.getUDPPort());
			channel.socket().bind(isa);
			channel.configureBlocking(false);
			channel.register(selector, SelectionKey.OP_READ);
			LogHelper.log(logger, Level.INFO, "UDP server bound to %s",isa.toString());
			while (true) {
				try {
					selector.select(selectedKey->{
						try {
							if (selectedKey.isValid() && selectedKey.isReadable()) {
								ByteBuffer receiveBuffer = ByteBuffer.allocate(RECEIVE_BUFFER_SIZE);
								InetSocketAddress remoteAddress = (InetSocketAddress)channel.receive(receiveBuffer);
								receiveBuffer.rewind();
								UplinkMessage message = UplinkMessage.toUplinkMessage(receiveBuffer);
								if (message!=null) {
									//  System.out.printf("got: p=%f t=%f h=%f, nvalves=%d (%s), uptime=%s from %02x:%02x:%02x:%02x:%02x:%02x - %s%n", message.pressure, message.temperature, message.humidity, message.valvesCount, Integer.toBinaryString(message.valvesBits), TimeInterval.valueOf(message.uptimeMilliseconds).toString(), message.controllerId[0],message.controllerId[1],message.controllerId[2],message.controllerId[3],message.controllerId[4],message.controllerId[5], remoteAddress.toString());
									scheduler.onUplinkPacket(message,remoteAddress);
								}
							}
						} catch (IOException e) {
							LogHelper.logException(logger, Level.WARNING, e, "Ignored excetion in UDP thread");
						}
					});
				} catch (IOException e) {
					LogHelper.logException(logger, Level.WARNING, e, "Ignored excetion in UDP thread");
				}
			}
		} catch (IOException e) {
			LogHelper.logException(logger, Level.SEVERE, e, "Fatal excetion in UDP thread");
		}
	}	

}
