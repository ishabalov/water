package org.shabalov.water.io;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;

public class TestIO {


    public static void main(String argv[]) throws InterruptedException {
    	final GpioController gpio = GpioFactory.getInstance();
    	// provision gpio pins #04 as an output pin and make sure is is set to LOW at startup
    	GpioPinDigitalOutput myLed = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_04,   // PIN NUMBER
    	                                                           "My LED",           // PIN FRIENDLY NAME (optional)
    	                                                           PinState.LOW);      // PIN STARTUP STATE (optional)
    	
    	while (true) {
    		myLed.high();
			Thread.sleep(1000);
    		myLed.low();
			Thread.sleep(1000);
    	}
    }
}
