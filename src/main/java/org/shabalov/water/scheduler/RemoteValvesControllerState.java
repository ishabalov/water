package org.shabalov.water.scheduler;

public enum RemoteValvesControllerState {
	offline,online,disabled;
	
	public String format() {
		switch (this) {
		case offline: return "OFF";
		case online: return "ON";
		case disabled : return "DIS";
		default: return "DIS";
		}
	}
	
}
