package org.shabalov.water.scheduler;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jbits.log.LogHelper;
import org.jbits.text.ConfigParserEntry;
import org.jbits.text.TextHelper;

public abstract class AbstractLine implements Externalizable {

	public final String id;
	public final String name;
	protected final Duration duration;
	protected final Logger logger;
	protected boolean isActive;
	protected Instant lastOn = null;
	private final Path lineStatePath;
	protected EnabledState activityState;
	protected LineState lineState;
	protected Duration lastOnDuration = null;

	private static final Duration DEFAULT_LINE_DURATION = Duration.ofMinutes(10);
	
	protected AbstractLine(Logger logger, String id) throws ClassNotFoundException {
		this.id=id;
		this.name=String.format("%s",id);
		this.lineState=LineState.off;
		this.logger=logger;
		this.duration=DEFAULT_LINE_DURATION;
		this.activityState=EnabledState.disabled;
		lineStatePath = Paths.get(id+".line.state");
		loadState();
	}
	
	protected AbstractLine(Logger logger, ConfigParserEntry entry) throws ClassNotFoundException {
		this.id=entry.getId();
		this.name = entry.getProperty(PROP_NAME);
		this.isActive = TextHelper.nullSafeToBooleanExtended(entry.getProperty(PROP_ACTIVE), false);
		EnabledState activityState;
		if (isActive) {
			activityState = EnabledState.enabled; 
		} else {
			activityState = EnabledState.disabled; 
		}
		this.activityState = activityState;
		this.lineState=LineState.off;
		this.logger=logger;
		this.duration=TextHelper.nullSafeToDuration(entry.getProperty(PROP_DURATION), Scheduler.DEFAULT_MANUAL_LINE_RUN);
		lineStatePath = Paths.get(id+".line.state");
		loadState();
	}

	protected void loadState() throws ClassNotFoundException {
		StateHelper.loadState(this, lineStatePath, logger);
	}

	protected void saveState() {
		StateHelper.saveState(this, lineStatePath, logger);
	}
	
	protected static final boolean isRemote(ConfigParserEntry entry) {
		return TextHelper.nullSafeEqualsIgnoreCase("remote", entry.getProperty(PROP_TYPE));
	}
	
	private static final String PROP_TYPE = "type";
	private static final String PROP_NAME = "name";
	private static final String PROP_ACTIVE = "active";
	private static final String PROP_DURATION = "duration";

	public LineState getLineState() {
		return lineState;
	}
	public EnabledState getActivityState() {
		return activityState;
	}
	public boolean isActive() {
		return isActive;
	}
	public boolean isOn() {
		switch(lineState) {
		case on:
		case stopping:
			return true;
		default: 
			return false;
		}
	}
	
	protected void setLineState(LineState lineState) {
		switch (lineState) {
		case on:
			switch (this.lineState) {
			case off:
			case stopping:
			case pending:
			case starting:
				this.lastOn = Instant.now();
				break;
			default:
			}
			break;
		case off:
		case stopping:
			switch (this.lineState) {
			case on:
			case starting:
				if (lastOn!=null) {
					this.lastOnDuration = Duration.between(lastOn, Instant.now());
				}
				break;
			default:
			}
			break;
		default:
		}
		this.lineState=lineState;
		switch (lineState) {
		case off:
		case on:
			saveState();
		break;
		default:
		}
	}

	abstract void internalTurnLineOn(); 
	abstract void internalTurnLineOff(); 
	
	protected void turnLineOnByProgram(Program program) {
		internalTurnLineOn();
		setLineState(LineState.on);
		LogHelper.log(this.logger,  Level.INFO, "Line '%s' turned ON by program '%s'",name,program.name);
	}

	protected void turnLineOffByProgram(Program program) {
		internalTurnLineOff();
		setLineState(LineState.off);
		LogHelper.log(this.logger,  Level.INFO, "Line '%s' turned OFF by program '%s'",name,program.name);
	}

	private void lineDelay() {
		try {
			Thread.sleep(Scheduler.LINE_DELAY.toMillis());
		} catch (InterruptedException e) {
			// ignore it
		}
	}
	
	protected void turnLineOffWithDelay(Program program) {
		setLineState(LineState.stopping);
		internalTurnLineOff();
		lineDelay();
		setLineState(LineState.off);
		LogHelper.log(this.logger,  Level.INFO, "Line '%s' turned OFF by program '%s'",name,program.name);
	}
	
	public boolean isEnabled() {
		switch (activityState) {
		case enabled: return duration!=null && !duration.isZero() && !duration.isNegative();
		default: return false;
		}
	}
	public void enableLine() {
		this.activityState=EnabledState.enabled;
	}
	public void toggleEnable() {
		switch (activityState) {
		case enabled:
			activityState=EnabledState.disabled;
			break;
		case disabled:
			activityState=EnabledState.enabled;
		}
	}
	public LineStat getLineStat() {
		if (LineState.on.equals(lineState)) {
			return new LineStat(lastOn, Duration.between(lastOn, Instant.now()));
		} else {
			return new LineStat(lastOn, lastOnDuration);
		}
	}

//	private Instant lastOn = null;
//	private Duration lastOnDuration = null;
	
	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		if (lastOn!=null) {
			out.writeLong(lastOn.toEpochMilli());
		} else {
			out.writeLong(0);
		}
		if (lastOnDuration!=null) {
			out.writeLong(lastOnDuration.toMillis());
		} else {
			out.writeLong(0);
		}
	}
	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		this.lastOn = Instant.ofEpochMilli(in.readLong());
		this.lastOnDuration = Duration.ofMillis(in.readLong());
	}
}
