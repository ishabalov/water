package org.shabalov.water.scheduler;

import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jbits.log.LogHelper;

public class InteractiveToggleLineProgram extends InteractiveProgram {
	private final AbstractLine line;
	private Instant toggleDeadline = null;
	
	private static final AtomicInteger NEXT_ID = new AtomicInteger();
	
	protected InteractiveToggleLineProgram(Logger logger, Scheduler scheduler, String name, AbstractLine line) {
		super(logger, scheduler, "TL_"+NEXT_ID.incrementAndGet(),  name);
		this.line = line;
	}

	@Override
	public String getStatLabel() {
		return String.format("%s - %s", name, line.name);
	}

	@Override
	protected void start() {
		LogHelper.log(scheduler.getLogger(), Level.INFO, "Toggle line '%s' executed: start", line.id);
		Program activeProgram = scheduler.getActiveProgram();
		if (activeProgram==null) {
			line.setLineState(LineState.starting);
			toggleLineOn();
		} else if (SchedulerProgram.class.isAssignableFrom(activeProgram.getClass())) {
			line.setLineState(LineState.starting);
			((SchedulerProgram)activeProgram).suspend();
			this.suspendedProgram = ((SchedulerProgram)activeProgram);
			toggleLineOn();
		} else if (InteractiveToggleLineProgram.class.isAssignableFrom(activeProgram.getClass()) &&  ((InteractiveToggleLineProgram)activeProgram).line.equals(this.line)) {
			toggleLineOff();
			((InteractiveToggleLineProgram)activeProgram).resumeSuspendedProgramIfNeeded();
		} else if (InteractiveProgram.class.isAssignableFrom(activeProgram.getClass())) {
			line.setLineState(LineState.starting);
			activeProgram.stop();
			this.suspendedProgram =((InteractiveProgram)activeProgram).suspendedProgram;
			toggleLineOn();
		}
	}

	private void toggleLineOff() {
		line.turnLineOffWithDelay(this);
		resumeSuspendedProgramIfNeeded();
	}

	private void toggleLineOn() {
		scheduler.setActiveProgram(this);
		line.turnLineOnByProgram(this);
		toggleDeadline = Instant.now().plus(line.duration);
	}

	@Override
	public ProgramState updateProgramState() {
		if (toggleDeadline!=null) {
			Instant now = Instant.now();
			if (now.isBefore(toggleDeadline)) {
				return new ProgramState(false, Duration.between(now, toggleDeadline));
			} else {
				toggleLineOff();
				return new ProgramState(true, Duration.ZERO);
			}
		} else {
			toggleLineOff();
			return new ProgramState(true, Duration.ZERO);
		}
	}

	@Override
	protected void stop() {
		line.turnLineOffWithDelay(this);
	}

}
