package org.shabalov.water.scheduler;

import java.time.Duration;
import java.time.Instant;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jbits.log.LogHelper;

public class RunAllLinesManualProgram extends InteractiveProgram {
	private Instant nextDeadLine = null;
	private AbstractLine activeLine = null;
	private Queue<AbstractLine> linesQueue = new LinkedBlockingQueue<>();

	private static final AtomicInteger NEXT_ID = new AtomicInteger();

	protected RunAllLinesManualProgram(Logger logger, Scheduler scheduler, String name) {
		super(logger, scheduler, "AL_"+NEXT_ID.incrementAndGet(),name);
	}

	@Override
	protected void start() {
		LogHelper.log(scheduler.getLogger(), Level.INFO, "Run all lines manual start");
		Program activeProgram = scheduler.getActiveProgram();
		if (activeProgram==null) {
			initProcess();
			lineOn(activeLine);
			scheduler.setActiveProgram(this);
		} else if (SchedulerProgram.class.isAssignableFrom(activeProgram.getClass())) {
			initProcess();
			((SchedulerProgram)activeProgram).suspend();
			this.suspendedProgram = ((SchedulerProgram)activeProgram);
			lineOn(activeLine);
			scheduler.setActiveProgram(this);
		} else if (RunAllLinesManualProgram.class.isAssignableFrom(activeProgram.getClass())) {
			((RunAllLinesManualProgram)activeProgram).stopProcess();
		} else if (InteractiveProgram.class.isAssignableFrom(activeProgram.getClass())) {
			initProcess();
			activeProgram.stop();
			this.suspendedProgram =((InteractiveProgram)activeProgram).suspendedProgram;
			lineOn(activeLine);
			scheduler.setActiveProgram(this);
		}
	}

	private void initProcess() {
		for (AbstractLine line:scheduler.getSortedLines()) {
			if (line.isEnabled()) {
				linesQueue.add(line);
			}
		}
		activeLine = linesQueue.poll();
		if (activeLine!=null) {
			activeLine.setLineState(LineState.starting);
		}
	}



	private void lineOn(AbstractLine line) {
		if (line!=null) {
			nextDeadLine = Instant.now().plus(line.duration);
			line.turnLineOnByProgram(this);
		}
	}

	private void stopProcess() {
		if (activeLine!=null) {
			activeLine.turnLineOffWithDelay(this);
		}
		resumeSuspendedProgramIfNeeded();
	}


	@Override
	public ProgramState updateProgramState() {
		if (activeLine!=null && nextDeadLine!=null) {
			Instant now = Instant.now();
			if (now.isBefore(nextDeadLine)) {
				return new ProgramState(false, Duration.between(now, nextDeadLine));
			} else {
				AbstractLine stoppingLine = activeLine;
				activeLine = linesQueue.poll();
				if (activeLine!=null) {
					activeLine.setLineState(LineState.starting);
				}
				stoppingLine.turnLineOffWithDelay(this);
				if (activeLine!=null) {
					nextDeadLine = Instant.now().plus(activeLine.duration);
					activeLine.turnLineOnByProgram(this);
					return new ProgramState(false, Duration.between(now, nextDeadLine));
				} else {
					resumeSuspendedProgramIfNeeded();
					return new ProgramState(true, Duration.ZERO);
				}
			}
		} else {
			return new ProgramState(true, Duration.ZERO);
		}
	}

	@Override
	protected void stop() {
		if (activeLine!=null) {
			activeLine.turnLineOffWithDelay(this);
		}
	}

}
