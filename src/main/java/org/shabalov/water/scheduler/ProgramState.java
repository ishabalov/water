package org.shabalov.water.scheduler;

import java.time.Duration;

public class ProgramState {
	protected final boolean isFinished;
	protected final Duration needAttentionAfter;
	protected ProgramState(boolean isFinished, Duration needAttentionAfter) {
		this.isFinished = isFinished;
		this.needAttentionAfter = needAttentionAfter;
	}
}
