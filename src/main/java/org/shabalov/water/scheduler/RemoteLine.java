package org.shabalov.water.scheduler;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jbits.log.LogHelper;
import org.jbits.text.ConfigParserEntry;

public class RemoteLine extends AbstractLine {

	protected final RemoteValvesController controller;
	protected final int valveIndex;

	private static final Pattern NAME_PATTERN = Pattern.compile("([^\\.]+)\\.(\\d+)");
	protected static final String toLineId(String controllerId, int valveIndex) {
		return String.format("%s.%d", controllerId,valveIndex);
	}
	
	protected RemoteLine(Logger logger, RemoteValvesController controller, int valveIndex) throws ClassNotFoundException {
		super(logger,toLineId(controller.id, valveIndex));
		this.controller=controller;
		this.valveIndex=valveIndex;
		controller.addLine(this);
	}
	
	protected RemoteLine(Logger logger, ConfigParserEntry entry, Map<String,RemoteValvesController> controllers) throws ClassNotFoundException {
		super(logger,entry);
		Matcher matcher = NAME_PATTERN.matcher(id);
		if (matcher.matches()) {
			String controllerId = matcher.group(1);
			this.valveIndex = Integer.valueOf(matcher.group(2));
			controller = controllers.get(controllerId);
			if (controller!=null) {
				controller.addLine(this);
			} else {
				LogHelper.log(this.logger,  Level.INFO, "Line '%s' has referenced missing controller: '%s', line is disabled",id,controllerId);
				activityState = EnabledState.disabled; 
			}
		} else {
			LogHelper.log(this.logger,  Level.INFO, "Line '%s' has invalid id format, line is disabled",id);
			controller=null;
			valveIndex=-1;
			activityState = EnabledState.disabled; 
		}
	}

	public void reconsileState(boolean isOnNow) {
		if (LineState.on.equals(lineState)) {
			internalTurnLineOn();
		} else if (LineState.off.equals(lineState) && isOnNow) {
			internalTurnLineOff();
		}
	}

	@Override
	void internalTurnLineOn() {
		controller.turnValveOn(valveIndex);
	}
	@Override
	void internalTurnLineOff() {
		controller.turnValveOff(valveIndex);
	}
	@Override
	public boolean isEnabled() {
		boolean enabled = super.isEnabled();
		if (enabled) {
			return controller.isOnline();
		} else {
			return false;
		}
	}
	
}
