package org.shabalov.water.scheduler;

public enum EnabledState {
	enabled,disabled;
	
	public String format() {
		switch (this) {
		case enabled: return "ENA";
		default:
		case disabled: return "DIS";
		}
	}
}
