package org.shabalov.water.scheduler;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jbits.log.LogHelper;
import org.jbits.text.ConfigParserEntry;
import org.jbits.text.TextHelper;
import org.jbits.time.TimeInterval;
import org.shabalov.water.config.ConfigHelper;
import org.shabalov.water.udp.Downlink;
import org.shabalov.water.udp.DownlinkMessage;
import org.shabalov.water.udp.UplinkMessage;

public class RemoteValvesController {

	private static final String PROP_NAME = "name";
	private static final String PROP_MAC = "mac";
	private static final String PROP_ACTIVE = "enabled";
	private static final String PROP_VALVE_ON_INTERVAL = "valveOnInterval";
	private static final String PROP_OFFLINE_TIMEOUT = "offlineTimeout";
	private static final boolean DEFAULT_ACTIVE = true;

	private final Duration DEFAULT_ON_INTERVAL = Duration.ofSeconds(10);
	private final Duration DEFAULT_OFFLINE_TIMEOUT = Duration.ofMinutes(1);

	public final String id;
	public final String name;
	public final String mac;
	public final boolean isActive;
	protected final Duration valveOnInterval;
	protected final Duration offlineTimeout;
	protected Map<Integer,RemoteLine> lines;

	private RemoteValvesControllerState state;
	private EnabledState enabledState;
	private Instant lastSeenAt;
	private int numberOfValves;
	private InetAddress lastSeenInetAddress;
	private Duration lastSeenUptime;
	private boolean[] lastSeenValveStatus;
	private double lastSeenPressure;
	private double lastSeenTemperrature;
	private double lastSeenHumidity;

	private final Downlink downlink;

	public RemoteValvesController(String mac, String name, Downlink downlink) {
		this.id = mac;
		this.mac = mac;
		this.name = name;
		this.downlink = downlink;
		this.valveOnInterval=DEFAULT_ON_INTERVAL;
		this.offlineTimeout=DEFAULT_OFFLINE_TIMEOUT;
		this.isActive=DEFAULT_ACTIVE;
		this.state = RemoteValvesControllerState.offline;
		this.lines = new HashMap<>();
		this.lastSeenAt = null;
		this.numberOfValves = 0;
		this.lastSeenInetAddress = null;
		this.lastSeenUptime = null;
		this.lastSeenValveStatus = null;
	}

	protected RemoteValvesController(Logger logger, ConfigParserEntry entry, Downlink downlink) throws ClassNotFoundException {
		this.id = entry.getId();
		this.name = entry.getProperty(PROP_NAME);
		this.mac = entry.getProperty(PROP_MAC); 
		this.downlink = downlink;
		this.valveOnInterval = nullSafeToDuration(entry.getProperty(PROP_VALVE_ON_INTERVAL), DEFAULT_ON_INTERVAL);
		this.offlineTimeout = nullSafeToDuration(entry.getProperty(PROP_OFFLINE_TIMEOUT), DEFAULT_OFFLINE_TIMEOUT);
		this.isActive = TextHelper.nullSafeToBooleanExtended(entry.getProperty(PROP_ACTIVE), DEFAULT_ACTIVE); 
		if (isActive) {
			this.state = RemoteValvesControllerState.offline;
			this.enabledState=EnabledState.enabled;
		} else {
			this.state = RemoteValvesControllerState.disabled;
			this.enabledState=EnabledState.disabled;
		}
		this.lines = new HashMap<>();
	}

	private static final Duration nullSafeToDuration(String str, Duration defaultValue) {
		if (!TextHelper.isNullOrEmpty(str)) {
			return TimeInterval.valueOf(str).toDuration();
		} else {
			return defaultValue;
		}
	}

	protected void addLine(RemoteLine line) {
		this.lines.put(line.valveIndex, line);
	}

	private InetSocketAddress getInetSocketAddress() {
		if (lastSeenInetAddress!=null) {
			return new InetSocketAddress(lastSeenInetAddress, ConfigHelper.getUDPPort());
		} else {
			return null;
		}
	}

	public synchronized void onUplinkPacket(Logger logger, UplinkMessage packet, InetSocketAddress remote) throws IOException {
		lastSeenAt = Instant.now();
		if (packet.valvesCount!=numberOfValves) {
			numberOfValves = packet.valvesCount;
			this.lastSeenValveStatus = new boolean[numberOfValves];
		}
		int mask = 0b01;
		int index=0;
		while (index<numberOfValves) {
			lastSeenValveStatus[index] = (packet.valvesBits & mask) != 0; 
			mask = mask << 1;
			index++;
		}
		this.lastSeenUptime = Duration.ofMillis(packet.uptimeMilliseconds);
		this.lastSeenPressure = packet.pressure;
		this.lastSeenTemperrature = packet.temperature;
		this.lastSeenHumidity = packet.humidity;
		this.lastSeenInetAddress = remote.getAddress();
		if (RemoteValvesControllerState.offline.equals(state)) {
			LogHelper.log(logger, Level.WARNING, "Controller '%s' online",id);
			this.state = RemoteValvesControllerState.online;
		}
		reconsileLinesState();
		downlink.sendCommand(getInetSocketAddress(), DownlinkMessage.ping());
	}

	private void reconsileLinesState() {
		for (int index=0;index<numberOfValves;index++) {
			RemoteLine line = lines.get(index);
			if (line!=null) {
				line.reconsileState(lastSeenValveStatus[index]);
			}
		}
	}

	public Instant getLastSeenAt() {
		return lastSeenAt;
	}

	public int getNumberOfValves() {
		return numberOfValves;
	}

	public InetAddress getLastSeenInetAddress() {
		return lastSeenInetAddress;
	}

	public Duration getLastSeenUptime() {
		return lastSeenUptime;
	}

	public boolean[] getLastSeenValveStatus() {
		return lastSeenValveStatus;
	}

	public double getLastSeenPressure() {
		return lastSeenPressure;
	}

	public double getLastSeenTemperrature() {
		return lastSeenTemperrature;
	}

	public double getLastSeenHumidity() {
		return lastSeenHumidity;
	}

	public RemoteValvesControllerState getState() {
		return state;
	}

	public boolean isOnline() {
		return RemoteValvesControllerState.online.equals(state);
	}

	public boolean isEnabled() {
		switch (state) {
		case disabled:
		case offline:
			return false;
		default:
			return true;
		}
	}


	public void turnValveOff(int valveIndex) {
		try {
			downlink.sendCommand(getInetSocketAddress(), DownlinkMessage.off(valveIndex));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void turnValveOn(int valveIndex) {
		try {
			downlink.sendCommand(getInetSocketAddress(), DownlinkMessage.toggle(valveIndex,(int)valveOnInterval.toMillis()));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void trackRemoteControllerState(Logger logger) {
		if (isOnline()) {
			Duration intervalFromLastSeen = Duration.between(lastSeenAt, Instant.now());
			if (this.offlineTimeout.compareTo(intervalFromLastSeen)<0) {
				this.state = RemoteValvesControllerState.offline;
				LogHelper.log(logger, Level.WARNING, "Controller '%s' offline by timeout",id);
			}
		}
	}

	public void sendResetRequest() {
		try {
			downlink.sendCommand(getInetSocketAddress(), DownlinkMessage.reset());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public EnabledState getEnabledState() {
		return enabledState;
	}

}
