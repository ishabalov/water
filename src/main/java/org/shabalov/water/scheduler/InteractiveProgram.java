package org.shabalov.water.scheduler;

import java.time.LocalDateTime;
import java.util.logging.Logger;

public abstract class InteractiveProgram extends Program {

	protected SchedulerProgram suspendedProgram = null;

	protected InteractiveProgram(Logger logger, Scheduler scheduler, String id, String name) {
		super(logger, scheduler, id, name);
	}

	protected void resumeSuspendedProgramIfNeeded() {
		if (suspendedProgram!=null) {
			scheduler.setActiveProgram(suspendedProgram);
			suspendedProgram.resume();
		} else {
			scheduler.setActiveProgram(null);
		}
	}

	@Override
	public LocalDateTime getLastRunDateTime() {
		return null;
	}
	
}
