package org.shabalov.water.scheduler;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jbits.log.LogHelper;
import org.shabalov.water.config.ConfigHelper;

public class StateHelper {

	private static final Path getSatePath(Path path) {
		return ConfigHelper.getDataRoot().resolve(path);
	}
	
	protected static final <T extends Externalizable> void loadState(T data, Path relativePath, Logger logger) throws ClassNotFoundException {
		try (ObjectInputStream is = new ObjectInputStream(Files.newInputStream(getSatePath(relativePath)))) {
			data.readExternal(is);
		} catch (NoSuchFileException e) {
			saveState(data, relativePath, logger);
		} catch (IOException e) {
			LogHelper.logException(logger, Level.WARNING, e, "Exception while loading state, ignored, path:'%s'",relativePath);
		}
	}
	
	protected static final <T extends Externalizable> void saveState(T data, Path relativePath, Logger logger) {
		try (ObjectOutputStream os = new ObjectOutputStream(Files.newOutputStream(getSatePath(relativePath), StandardOpenOption.CREATE))) {
			if (data!=null) {
				data.writeExternal(os);
			}
		} catch (IOException e) {
			LogHelper.logException(logger, Level.WARNING, e, "Exception while saving state, ignored");
		}
	}

}
