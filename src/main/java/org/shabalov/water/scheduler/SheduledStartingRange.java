package org.shabalov.water.scheduler;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SheduledStartingRange {
	protected final LocalTime from;
	protected final LocalTime to;
	
	private static final Pattern PARSER = Pattern.compile("(\\d+\\:\\d+)?(\\-(\\d+\\:\\d+)?)?");
	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("HH:mm");
	
	protected SheduledStartingRange(String definition) {
		Matcher matcher = PARSER.matcher(definition);
		if (matcher.matches()) {
			String fromStr = matcher.group(1);
			if (fromStr!=null) {
				this.from = LocalTime.parse(fromStr, FORMATTER);
			} else {
				this.from = null;
			}
			String toStr = matcher.group(3);
			if (toStr!=null) {
				this.to = LocalTime.parse(toStr, FORMATTER);
			} else {
				this.to = null;
			}
		} else {
			throw new RuntimeException(String.format("Incorrect program starting range definition: '%s' ",definition));
		}
	}

	protected boolean isInRange(LocalDateTime time) {
		LocalTime lt = time.toLocalTime();
		if (from!=null) {
			if (from.compareTo(lt)>0) {
				return false;
			}
		}
		if (to!=null) {
			if (to.compareTo(lt)<=0) {
				return false;
			}
		}
		return true;
	}
	
}
