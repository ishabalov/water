package org.shabalov.water.scheduler;

import java.time.LocalDateTime;

public class SchedulerProgramStat {

	public final String name;
	public final LocalDateTime timestamp;
	protected SchedulerProgramStat(String name, LocalDateTime timestamp) {
		this.name = name;
		this.timestamp = timestamp;
	}
}
