package org.shabalov.water.scheduler;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.jbits.log.LogHelper;
import org.jbits.text.ConfigParserEntry;
import org.jbits.text.TextHelper;
import org.shabalov.water.gpio.GpioHandler;

public class LocalLine extends AbstractLine {

	protected final int gpioChannel;
	protected final GpioHandler gpioHandler;
	

	protected LocalLine(Logger logger, ConfigParserEntry entry, GpioHandler gpioHandler) throws ClassNotFoundException {
		super(logger,entry);
		this.gpioChannel = TextHelper.nullSafeToInt(entry.getProperty(PROP_GPIO),DEFAULT_GPIO);
		if (isActive && gpioChannel>=0 && gpioChannel<GpioHandler.MAX_GPIO_CHANNELS) {
			activityState = EnabledState.enabled; 
		} else {
			activityState = EnabledState.disabled; 
		}
		this.gpioHandler=gpioHandler;
		if (EnabledState.enabled.equals(activityState)) {
			LogHelper.log(this.logger,  Level.INFO, "Line '%s' is activated on valve %d",name,gpioChannel);
			this.gpioHandler.turnOff(gpioChannel);
		} else {
			LogHelper.log(logger,  Level.INFO, "Line '%s' is disabled on valve %d",name,gpioChannel);
		}
	}

	private static final String PROP_GPIO = "gpio";
	private static final int DEFAULT_GPIO=-1;

	@Override
	protected void internalTurnLineOn() {
		this.gpioHandler.turnOn(gpioChannel);
	}

	@Override
	protected void internalTurnLineOff() {
		this.gpioHandler.turnOff(gpioChannel);
	}

}
