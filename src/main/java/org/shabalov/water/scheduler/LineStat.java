package org.shabalov.water.scheduler;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class LineStat {
	public final LocalDateTime lastOn;
	public final Duration lastOnDuration;
	protected LineStat(Instant lastOn, Duration lastOnDuration) {
		this.lastOn = lastOn!=null?LocalDateTime.ofInstant(lastOn, ZoneId.systemDefault()):null;
		this.lastOnDuration = lastOnDuration;
	}
}	
