package org.shabalov.water.scheduler;

public enum LineState {
	on,off,starting,stopping,pending;
	
	public String format() {
		switch (this) {
		case on: return "ON";
		case starting: return "STA";
		case stopping: return "STO";
		case pending: return "PEN";
		default: return "OFF";
			
		}
	}
}
