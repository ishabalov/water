package org.shabalov.water.scheduler;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jbits.collections.CollectionsHelper;
import org.jbits.log.LogHelper;
import org.jbits.text.ConfigParserEntry;
import org.jbits.text.TextHelper;
import org.jbits.utils.AtomicReferencePlus;
import org.jbits.utils.NumberHelper;

public class SchedulerProgram extends Program implements Externalizable {
	protected final boolean isActive;
	protected final int priorityOrder;
	protected final List<AbstractLine> lines;
	protected final Duration interval;
	protected final List<SheduledStartingRange> startingRanges;
	protected final List<DayOfWeek> weekDays;
	protected final double durationMultiplier;

	protected ProgramRunState runState;
	protected EnabledState enabledState;
	private final Path programPath;
	
	protected Instant lastStart = null;
	
	private Queue<AbstractLine> linesQueue = null;
	private AbstractLine activeLine = null;
	private Instant nextDeadline = null;
	private Duration activeLineResumeReminder = null;
	
	public static final String PROP_NAME = "name";
	public static final String PROP_LINES = "lines";
	public static final String PROP_INTERVAL = "interval";
	public static final String PROP_STARTING = "starting";
	public static final String PROP_WEEK = "week";
	public static final String PROP_DURATION_MULTIPLIER = "durationMultiplier";
	private static final String PROP_ACTIVE = "active";
	public static final double DEFAULT_DURATION_MULTIPLIER = 1.0;
	public static final boolean DEFAULT_ACTIVE = false;

	private static final Pattern SPLIT = Pattern.compile("[\\,\\s]+");

	
	/*
			[garden]
			name=Veggy Garden
			enabled=yes
			lines=l_01,l_02,l_03
			interval=12h
			starting=06:00-08:00,18:00-00:00
			durationMultiplier=0.5	
			week=su,m,tu,w,th,f,sa
	*/
	protected SchedulerProgram(Logger logger, Scheduler scheduler,int priorityOrder, ConfigParserEntry entry) throws ClassNotFoundException {
		super(logger,scheduler,entry.getId(), entry.getProperty(PROP_NAME));
		this.isActive=TextHelper.nullSafeToBooleanExtended(entry.getProperty(PROP_ACTIVE), DEFAULT_ACTIVE);
		if (isActive) {
			this.enabledState=EnabledState.enabled;
			this.runState=ProgramRunState.idle;
		} else {
			this.enabledState=EnabledState.disabled;
			this.runState=ProgramRunState.disabled;
		}
		this.priorityOrder=priorityOrder;
		this.lines = parseLines(scheduler,entry.getProperty(PROP_LINES));  
		this.interval = parseInterval(entry.getProperty(PROP_INTERVAL));
		this.startingRanges = parseStartingRanges(entry.getProperty(PROP_STARTING));
		this.weekDays = parseWeekDays(entry.getProperty(PROP_WEEK));
		this.durationMultiplier =  NumberHelper.nullSafeToDouble(entry.getProperty(PROP_DURATION_MULTIPLIER),DEFAULT_DURATION_MULTIPLIER);
		if (interval!=null) {
			LogHelper.log(logger, Level.INFO, "Loaded program '%s', interval %s with lines %s", this.name, this.interval, lines.stream().map((l)->"'"+l.name+"'").collect(Collectors.joining(",")));
		} else {
			LogHelper.log(logger, Level.INFO, "Loaded program '%s', is inactive", this.name);
		}
		this.programPath = Paths.get(id+".program.state");
		StateHelper.loadState(this, programPath, logger);
	}

	private static final List<DayOfWeek> parseWeekDays(String property) {
		List<DayOfWeek> ret = new ArrayList<>();
		if (!TextHelper.isNullOrEmpty(property)) {
			for (String ln:SPLIT.split(property)) {
				ret.add(parseWeekDay(ln.toLowerCase()));
			}
		}
		return ret;
	}
	
	private static final DayOfWeek parseWeekDay(String ln) {
		if (ln.startsWith("su")) {
			return DayOfWeek.SUNDAY;
		} else if (ln.startsWith("m")) {
			return DayOfWeek.MONDAY;
		} else if (ln.startsWith("tu")) {
			return DayOfWeek.TUESDAY;
		} else if (ln.startsWith("w")) {
			return DayOfWeek.WEDNESDAY;
		} else if (ln.startsWith("th")) {
			return DayOfWeek.THURSDAY;
		} else if (ln.startsWith("f")) {
			return DayOfWeek.FRIDAY;
		} else if (ln.startsWith("sa")) {
			return DayOfWeek.SATURDAY;
		} else {
			throw new RuntimeException(String.format("Illegal week day definition: '%s'",ln));
		}
	}

	private static final List<SheduledStartingRange> parseStartingRanges(String property) {
		List<SheduledStartingRange> ret = new ArrayList<>();
		if (!TextHelper.isNullOrEmpty(property)) {
			for (String ln:SPLIT.split(property)) {
				ret.add(new SheduledStartingRange(ln));
			}
		}
		return ret;
	}

	private static final Duration parseInterval(String property) {
		return TextHelper.nullSafeToDuration(property, null);
	}

	private final List<AbstractLine> parseLines(Scheduler scheduler,String property) {
		List<AbstractLine> ret = new ArrayList<>();
		if (!TextHelper.isNullOrEmpty(property)) {
			for (String ln:SPLIT.split(property)) {
				AbstractLine line = scheduler.getLines().get(ln);
				if (line!=null) {
					ret.add(line);
				} else {
					LogHelper.log(logger, Level.WARNING, "Line reference '%s' for program '%s' is missing in lines definition file, line ignored", ln, name);
				}
			}
		}
		return ret;
	}

	protected void suspend() {
		LogHelper.log(logger, Level.INFO, "Scheduler program '%s' suspend", name);
		if (activeLine!=null) {
			activeLineResumeReminder = Duration.between(Instant.now(), nextDeadline);
			activeLine.turnLineOffWithDelay(this);
		}
		runState=ProgramRunState.suspended;
		LogHelper.log(logger, Level.INFO, "Scheduler program '%s' suspended, active line: '%s' reminder: %s", name, activeLine.name, activeLineResumeReminder);
	}

	protected void resume() {
		LogHelper.log(logger, Level.INFO, "Scheduler program '%s' resume", name);
		if (activeLine!=null) {
			if (activeLineResumeReminder.isNegative() || activeLineResumeReminder.isZero()) {
				activeLine = linesQueue.poll();
				if (activeLine!=null) {
					activeLine.turnLineOnByProgram(this);
					setNextDeadlineForActiveLine();
				}
			} else {
				activeLine.turnLineOnByProgram(this);
				nextDeadline = Instant.now().plus(activeLineResumeReminder);
			}
		}
		runState=ProgramRunState.running;
		LogHelper.log(logger, Level.INFO, "Scheduler program '%s' resumed, active line: '%s' deadline: %s", name, activeLine.name, nextDeadline);
	}

	@Override
	public ProgramState updateProgramState() {
		if (activeLine!=null) {
			if (Instant.now().isAfter(nextDeadline)) {
				// next line
				AbstractLine stoppingLine = activeLine;
				activeLine = linesQueue.poll();
				if (activeLine!=null) {
					activeLine.setLineState(LineState.starting);
				}
				stoppingLine.turnLineOffWithDelay(this);
				if (activeLine!=null) {
					activeLine.turnLineOnByProgram(this);
					setNextDeadlineForActiveLine();
					return new ProgramState(false,  Duration.between(Instant.now(), nextDeadline));
				} else {
					runState=ProgramRunState.idle;
					LogHelper.log(logger, Level.INFO, "Scheduler program '%s' is finished", name);
					return new ProgramState(true, Duration.ZERO);
				}
			} else {
				return new ProgramState(false, Duration.between(Instant.now(), nextDeadline));
			}
		} else {
			runState=ProgramRunState.idle;
			return new ProgramState(true, Duration.ZERO);
		}
	}
	
	private Stream<AbstractLine> getEnabledLines() {
		return lines.stream().filter(l->l.isEnabled());
	}
	
	protected Duration getTotalDurationOfAllEnabedLines() {
		return Duration.ofSeconds(getEnabledLines().mapToLong(l->(long)(durationMultiplier*(double)l.duration.getSeconds())).reduce(0, (l,r)->l+r));
	}
	
	@Override
	public void start() {
		LogHelper.log(logger, Level.INFO, "Scheduler program '%s' started", name);
		lastStart = Instant.now();
		lastPeriodStarted.set(null);
		StateHelper.saveState(this, programPath, logger);
		linesQueue = new LinkedBlockingQueue<>();
		getEnabledLines().forEach(linesQueue::add);
		activeLine = linesQueue.poll();
		if (activeLine!=null) {
			setNextDeadlineForActiveLine();
			activeLine.turnLineOnByProgram(this);
			runState=ProgramRunState.running;
		} else {
			runState=ProgramRunState.idle;
		}
	}


	private void setNextDeadlineForActiveLine() {
		long nano = Math.round(durationMultiplier * (double)activeLine.duration.toNanos());
		nextDeadline = Instant.now().plus(nano, ChronoUnit.NANOS);
	}
	
	@Override
	protected void stop() {
		if (activeLine!=null) {
			activeLine.turnLineOffWithDelay(this);
		}
		activeLine = null;
		linesQueue = null;
		nextDeadline = null;
		activeLineResumeReminder = null;
		runState=ProgramRunState.idle;
		LogHelper.log(logger, Level.INFO, "Scheduler program '%s' stopped", name);
	}

	public boolean isEnabled() {
		switch (enabledState) {
		case disabled:
			return false;
		default:
			return interval!=null && !interval.isZero() && !interval.isNegative();
		}
	}
	
	public boolean isActive() {
		return isActive;
	}

	public boolean mayRunAtDateTime(LocalDateTime time) {
		return checkByLastStart(time) && checkByConstrains(time);
	}

	private boolean checkByLastStart(LocalDateTime time) {
		if (lastStart!=null) {
			ProgramPeriod period = new ProgramPeriod(interval,time);
			return   period.isAfter(getLastStartedPeriod());
		} else {
			return true;
		}
	}

	protected AtomicReferencePlus<ProgramPeriod> lastPeriodStarted = new AtomicReferencePlus<>();

	private ProgramPeriod getLastStartedPeriod() {
		return lastPeriodStarted.computeIfEmpty(()->new ProgramPeriod(interval,getLastRunDateTime()));
	}

	private boolean checkByConstrains(LocalDateTime time) {
		if (!CollectionsHelper.isNullOrEmpty(startingRanges)) {
			boolean hasOneInRange = false;
			for (SheduledStartingRange r:startingRanges) {
				if (r.isInRange(time)) {
					hasOneInRange = true;
					break;
				}
			}
			if (!hasOneInRange) {
				return false;
			}
		}
		if (!CollectionsHelper.isNullOrEmpty(weekDays)) {
			boolean hasOneWeekDay = false;
			DayOfWeek dn = time.getDayOfWeek();
			for (DayOfWeek wd:weekDays) {
				if (dn.equals(wd)) {
					hasOneWeekDay = true;
					break;
				}
			}
			return hasOneWeekDay;
		}
		return true;
	}

	@Override
	public LocalDateTime getLastRunDateTime() {
		return LocalDateTime.ofInstant(lastStart,ZoneId.systemDefault());
	}

	public boolean hasEverRun() {
		return lastStart!=null && lastStart.toEpochMilli()>0;
	}

	public boolean lastRunEarlierThan(SchedulerProgram b) {
		return 
			lastStart!=null && (lastStart.toEpochMilli()<=0 || (b.lastStart!=null && lastStart.isBefore(b.lastStart)));
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		if (lastStart!=null) {
			out.writeLong(lastStart.toEpochMilli());
		} else {
			out.writeLong(0);
		}
	}
	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		this.lastStart = Instant.ofEpochMilli(in.readLong());
	}

	public ProgramRunState getProgramRunState() {
		return runState;
	}
	
	public boolean isRunning() {
		switch (runState) {
		case running: return true;
		default: return false;
		}
	}

	public EnabledState getProgramEnabledState() {
		return enabledState;
	}

	public void toggleEnable() {
		switch (enabledState) {
		case enabled:
			enabledState=EnabledState.disabled;
			break;
		case disabled:
			enabledState=EnabledState.enabled;
			break;
		}
	}


	public boolean hasAtLeastOneActiveLine() {
		for (AbstractLine line :lines) {
			if (line.isEnabled()) {
				return true;
			}
		}
		return false;
	}


}
