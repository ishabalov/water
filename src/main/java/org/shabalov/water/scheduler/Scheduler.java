package org.shabalov.water.scheduler;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
//import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.jbits.beans.Inject;
import org.jbits.beans.PostConstruct;
import org.jbits.io.IoHelper;
import org.jbits.log.LogHelper;
import org.jbits.math.MathHelper;
import org.jbits.text.ConfigParser;
import org.jbits.text.ConfigParserEntry;
import org.jbits.text.TextHelper;
import org.shabalov.water.config.ConfigHelper;
import org.shabalov.water.gpio.GpioHandler;
import org.shabalov.water.udp.Downlink;
import org.shabalov.water.udp.UplinkMessage;

public class Scheduler implements Externalizable  {

	@Inject private Logger logger;
	@Inject private GpioHandler gpioHandler;
	@Inject private Downlink downlink;

	private Map<String,SchedulerProgram> programs=null;
	private Map<String,AbstractLine> lines=null;
	private Map<String,RemoteValvesController> remoteControllersById=null;
	private Map<String,RemoteValvesController> remoteControllersByMacAddress=null;
	private Duration manualLineDuration = DEFAULT_MANUAL_LINE_RUN;

	private SchedulerRunMode runMode = SchedulerRunMode.running;

	private final Path statePath = Paths.get("scheduler.state");

	public static final Duration THREAD_LOOP = Duration.ofMillis(1000);
	public static final Duration LINE_DELAY = Duration.ofMillis(5000);
	public static final Duration DEFAULT_MANUAL_LINE_RUN = Duration.ofSeconds(10);

	@PostConstruct
	protected void init() throws ClassNotFoundException {
		LogHelper.log(logger,  Level.INFO, "Init scheduler");
		loadConfig();
		programsThread = new Thread(getProgramsRunnable(), "Programs Thread");
		programsThread.setDaemon(false);
		programsThread.start();
		gpioHandler.blink(5);
	}

	protected void suspend() {
		this.runMode = SchedulerRunMode.suspended;
	}
	protected void resume() {
		this.runMode = SchedulerRunMode.running;
	}
	
	private Runnable getProgramsRunnable() {
		return new Runnable() {

			@Override
			public void run() {
				while (!isExitRequested()) {
					try {
						SchedulerCommand command = lastCommand.getAndSet(null);
						if (command!=null) {
							command.execute();
						}
						Program currentProgram = activeProgram.get();
						if (SchedulerRunMode.running.equals(runMode)) {
							if (currentProgram!=null) {
								ProgramState state = currentProgram.updateProgramState();
								if (!state.isFinished) {
									if (state.needAttentionAfter.toMillis()>0 && state.needAttentionAfter.toMillis()<=THREAD_LOOP.toMillis()) {
										park(state.needAttentionAfter.toMillis());
									} else {
										park(THREAD_LOOP.toMillis());
									}
								} else {
									// avoid resume case
									if (currentProgram.equals(activeProgram.get())) {
										lastRunProgramStat.set(getProgramStat(activeProgram.get())); 
										activeProgram.set(null);
										StateHelper.saveState(Scheduler.this, statePath, logger);
									}
								}
							} else {
								SchedulerProgram nextProgram = getNextSchedulerProgramToRunNow();
								activeProgram.set(nextProgram);
								if (nextProgram!=null) {
									nextProgram.start();
									gpioHandler.blink(1);
								}
								park(THREAD_LOOP.toMillis());
							}
						} else {
							// suspended operations
							if (currentProgram!=null) {
								currentProgram.stop();
								activeProgram.set(null);
							}
							nextProgramStat.set(null);
						}
						trackSchedulerConfig();
						trackAllRemoteControllersState();
					} catch (Exception e) {
						LogHelper.logException(logger, Level.WARNING, e, "Ignored exception in scheduler loop");
					}
				}
			}

			private void park(long milliseconds) {
				try {
					synchronized (programsThread) {
						if (lastCommand.get()==null) {
							programsThread.wait(milliseconds);
						}
					}
				} catch (InterruptedException e) {
					// do nothing
				}
			}

		};
	}

	private void trackAllRemoteControllersState() {
		remoteControllersById.values().forEach(c->c.trackRemoteControllerState(logger));
	}


	private static final long LOOK_FORWARD_SEC = Duration.ofDays(10).getSeconds();  
	private SchedulerProgram getNextSchedulerProgramToRunNow() {
		SchedulerProgram runNow = getSchedulerProgramToRunAtTime(LocalDateTime.now(),null);
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime base = LocalDateTime.of(now.getYear(),now.getMonth(),now.getDayOfMonth(),now.getHour(),now.getMinute(),now.getSecond(),0);
		long plusSeconds;
		if (runNow!=null) {
			plusSeconds = runNow.getTotalDurationOfAllEnabedLines().getSeconds();
		} else {
			plusSeconds = 0;
		}
		Program runNext = null;
		while (runNext==null && plusSeconds<=LOOK_FORWARD_SEC) {
			runNext = getSchedulerProgramToRunAtTime(base.plusSeconds(plusSeconds),runNow);
			plusSeconds++;
		}
		if (runNext!=null) {
			nextProgramStat.set(new SchedulerProgramStat(runNext.name, base.plusSeconds(plusSeconds)));
		} else {
			nextProgramStat.set(null);
		}
		return runNow;
	}

	private SchedulerProgram getSchedulerProgramToRunAtTime(LocalDateTime time, Program excludeProgram) {
		List<SchedulerProgram> programsThatMayRun = new ArrayList<>();
		for (SchedulerProgram program:programs.values()) {
			if (program.isEnabled() && program.mayRunAtDateTime(time)) {
				if (excludeProgram==null || !excludeProgram.equals(program)) {
					programsThatMayRun.add(program);
				}
			}
		}
		if (programsThatMayRun.isEmpty()) {
			return null;
		} else {
			SchedulerProgram ret = null;
			for (SchedulerProgram p:programsThatMayRun) {
				if (ret==null) {
					ret = p;
				} else {
					ret = getBetterProgramToRun(ret, p);
				}
			}
			return ret;
		}
	}

	private SchedulerProgram getBetterProgramToRun(SchedulerProgram a, SchedulerProgram b) {
		if (a.hasEverRun()) {
			if (b.hasEverRun()) {
				if (a.lastRunEarlierThan(b)) {
					return a;
				} else {
					return b;
				}
			} else {
				return b;
			}
		} else {
			return a;
		}
	}


	private AtomicBoolean isExitRequested = new AtomicBoolean(false);

	public boolean isExitRequested() {
		synchronized (programsThread) {
			return isExitRequested.get();
		}
	}

	public void requestExit() {
		synchronized(programsThread) {
			isExitRequested.set(true);
			programsThread.notifyAll();
		}
	}

	private AtomicReference<SchedulerCommand> lastCommand = new AtomicReference<>();
	private AtomicReference<Program> activeProgram = new AtomicReference<>();
	private AtomicReference<SchedulerProgramStat> lastRunProgramStat = new AtomicReference<>();
	private AtomicReference<SchedulerProgramStat> nextProgramStat = new AtomicReference<>();
	private Thread programsThread = null;

	private Map<String,SchedulerProgram> loadPrograms() throws ClassNotFoundException {
		Map<String,SchedulerProgram> ret = new HashMap<>();
		try {
			Path path = ConfigHelper.getProgramsConfigPath();
			LogHelper.log(logger, Level.INFO, "Loading programs configuration from '%s'", path.toAbsolutePath());
			String lineConfig = IoHelper.readFile(path, StandardCharsets.UTF_8);
			ConfigParser parser = ConfigParser.parse(lineConfig);
			int order = 0;
			for (ConfigParserEntry entry:parser.getEntries()) {
				SchedulerProgram program = new SchedulerProgram(logger, this, order, entry);
				ret.put(program.id,program);
				order++;
			}
		} catch (IOException e) {
			LogHelper.logException(logger, Level.SEVERE, e, "Exception while loading lines configuration, line configuration is missing");
		}
		return ret;
	}

	private Instant configLastLoadTime = null;
	private void trackSchedulerConfig() throws IOException, ClassNotFoundException {
		if (isConfigUpdated()) {
			Program activeProgram = getActiveProgram();
			if (activeProgram!=null) {
				activeProgram.stop();
				setActiveProgram(null);
			}
			loadConfig();
		}
	}
	private boolean isConfigUpdated() throws IOException {
		return 	configLastLoadTime==null || 
				configLastLoadTime.isBefore(Files.getLastModifiedTime(ConfigHelper.getLinesConfigPath()).toInstant()) ||
				configLastLoadTime.isBefore(Files.getLastModifiedTime(ConfigHelper.getProgramsConfigPath()).toInstant()) ||
				configLastLoadTime.isBefore(Files.getLastModifiedTime(ConfigHelper.getRemoteControllersConfigPath()).toInstant());
	}

	private void loadConfig() throws ClassNotFoundException {
		remoteControllersById = loadRemoteControllers();
		remoteControllersByMacAddress  = new HashMap<>();
		remoteControllersById.forEach((id,controller)->{
			if (!TextHelper.isNullOrEmpty(controller.mac)) {
				remoteControllersByMacAddress.put(controller.mac, controller);
			}
		});
		lines = loadLines();
		programs = loadPrograms();
		configLastLoadTime = Instant.now();
		StateHelper.loadState(this, statePath, logger);
	}

	private Map<String,RemoteValvesController> loadRemoteControllers() throws ClassNotFoundException {
		Map<String,RemoteValvesController> ret = new HashMap<>();
		try {
			Path path = ConfigHelper.getRemoteControllersConfigPath();
			LogHelper.log(logger, Level.INFO, "Loading remote controllers configuration from '%s'", path);

			String lineConfig = IoHelper.readFile(path, StandardCharsets.UTF_8);
			ConfigParser parser = ConfigParser.parse(lineConfig);
			for (ConfigParserEntry entry:parser.getEntries()) {
				RemoteValvesController line = new RemoteValvesController(logger,entry,downlink);
				ret.put(line.id,line);
			}
		} catch (IOException e) {
			LogHelper.logException(logger, Level.SEVERE, e, "Exception while loading remote controllers configuration, remote controller configuration is missing");
		}
		return ret;
	}

	private Map<String,AbstractLine> loadLines() throws ClassNotFoundException {
		Map<String,AbstractLine> ret = new HashMap<>();
		try {
			Path path = ConfigHelper.getLinesConfigPath();
			LogHelper.log(logger, Level.INFO, "Loading lines configuration from '%s'", path);

			String lineConfig = IoHelper.readFile(path, StandardCharsets.UTF_8);
			ConfigParser parser = ConfigParser.parse(lineConfig);
			for (ConfigParserEntry entry:parser.getEntries()) {
				AbstractLine line;
				if (AbstractLine.isRemote(entry)) {
					line = new RemoteLine(logger, entry, remoteControllersById);
				} else {
					line = new LocalLine(logger,entry,gpioHandler);
				}
				LogHelper.log(logger, Level.INFO, "Added line '%s'", line.id);
				ret.put(line.id,line);
			}
		} catch (IOException e) {
			LogHelper.logException(logger, Level.SEVERE, e, "Exception while loading lines configuration, line configuration is missing");
		}
		return ret;
	}

	public Map<String,AbstractLine> getLines() {
		return lines;
	}

	public Iterable<AbstractLine> getSortedLines() {
		return getLines().values().stream()
				.sorted((l1,l2) -> TextHelper.nullSafeCompareIgnoreCase(l1.id, l2.id))
				.collect(Collectors.toList());
	}

	public Iterable<SchedulerProgram> getSortedPrograms() {
		return programs.values().stream()
				.sorted((p1,p2)-> TextHelper.nullSafeCompareIgnoreCase(p1.id, p2.id))
				.collect(Collectors.toList());
	}

	public Iterable<RemoteValvesController> getSortedControllers() {
		return remoteControllersById.values().stream()
				.sorted((p1,p2)-> TextHelper.nullSafeCompareIgnoreCase(p1.id, p2.id))
				.collect(Collectors.toList());
	}

	private void executeManualLineToggle(AbstractLine line) {
		InteractiveToggleLineProgram newProgram = new InteractiveToggleLineProgram(logger, this, "One Line Manual", line);
		newProgram.start();
	}

	private void executeRunAllLinesManual() {
		RunAllLinesManualProgram newProgram = new RunAllLinesManualProgram(logger, this, "All Lines Manual");
		newProgram.start();
	}


	public void waitForExitRequest() {
		while (!isExitRequested()) {
			synchronized (programsThread) {
				try {
					programsThread.wait(THREAD_LOOP.toMillis());
				} catch (InterruptedException e) {
				}
			} 
		}
	}

	public Program getActiveProgram() {
		return activeProgram.get();
	}

	protected void setActiveProgram(Program program) {
		activeProgram.set(program);

	}

	public Duration getManualLineDuration() {
		return manualLineDuration;
	}

	public void setManualLineDuration(Duration manualLineDuration) {
		this.manualLineDuration = manualLineDuration;
	}

	protected Logger getLogger() {
		return logger;
	}

	public void toggleLineManualSubmit(String lineId) {
		final AbstractLine line = lines.get(lineId);
		LogHelper.log(logger, Level.INFO, "Toggle line '%s'", lineId);
		if (line!=null) {
			line.setLineState(LineState.pending);
			lastCommand.set(()->executeManualLineToggle(line));
			synchronized (programsThread) {
				programsThread.notify();
			}
			gpioHandler.blink(1);
		} else {
			throw new RuntimeException(String.format("Line id= '%s' does not exists", lineId));
		}
	}

	public void toggleEnableLineManualSubmit(String lineId) {
		final AbstractLine line = lines.get(lineId);
		LogHelper.log(logger, Level.INFO, "Toggle enable line '%s'", lineId);
		if (line!=null) {
			line.toggleEnable();
		} else {
			throw new RuntimeException(String.format("Line id= '%s' does not exists", lineId));
		}
	}

	public void runAllLinesManualSubmit() {
		LogHelper.log(logger, Level.INFO, "Run all lines manual pressed");
		lastCommand.set(()->executeRunAllLinesManual());
		synchronized (programsThread) {
			programsThread.notify();
		}
		gpioHandler.blink(1);
	}

	public SchedulerProgramStat getLastProgramStat() {
		return lastRunProgramStat.get();
	}
	public SchedulerProgramStat getActiveProgramStat() {
		return getProgramStat(activeProgram.get());
	}
	public SchedulerProgramStat getNextProgramStat() {
		return nextProgramStat.get();
	}
	private SchedulerProgramStat getProgramStat(Program program) {
		if (program!=null) {
			return new SchedulerProgramStat(program.getStatLabel(), program.getLastRunDateTime());
		} else {
			return null;
		}
	}

	private synchronized RemoteValvesController getOrCreateControllerByMacAddress(UplinkMessage packet) {
		String macAddress = packet.getControllerMacAddress();
		RemoteValvesController remoteController = remoteControllersByMacAddress.get(macAddress);
		if (remoteController==null) {
			remoteController = new RemoteValvesController(macAddress, macAddress, downlink);
			remoteControllersById.put(macAddress, remoteController);
			remoteControllersByMacAddress.put(macAddress,remoteController);
		}
		return remoteController;
	}

	public void onUplinkPacket(UplinkMessage packet,InetSocketAddress remoteAddress) throws IOException {
		RemoteValvesController remoteController = getOrCreateControllerByMacAddress(packet);
		for (int valveIndex=0;valveIndex<packet.valvesCount;valveIndex++) {
			String remoteLineId = RemoteLine.toLineId(remoteController.id, valveIndex);
			RemoteLine line = (RemoteLine) lines.get(remoteLineId);
			if (line==null) {
				try {
					line = new RemoteLine(logger,remoteController,valveIndex);
					lines.put(remoteLineId, line);
				} catch (ClassNotFoundException e) {
					throw new RuntimeException(e);
				}
			}
		}
		remoteController.onUplinkPacket(logger,packet,remoteAddress);
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		SchedulerProgramStat stat = lastRunProgramStat.get();
		if (stat!=null) {
			out.writeBoolean(true);
			out.writeUTF(stat.name);
			out.writeLong(stat.timestamp.toInstant(ZoneOffset.UTC).toEpochMilli());
		} else {
			out.writeBoolean(false);
		}
		out.writeUTF(runMode.name());
	}
	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		boolean hasLastRunProgram = in.readBoolean();
		if (hasLastRunProgram) {
			String name = in.readUTF();
			LocalDateTime timestamp = LocalDateTime.ofInstant(Instant.ofEpochMilli(in.readLong()), ZoneOffset.UTC);
			lastRunProgramStat.set(new SchedulerProgramStat(name, timestamp));
		}
		String runModeId = in.readUTF();
		runMode = SchedulerRunMode.fromName(runModeId);
	}

	public Double getRemoteControllersAverageTemperrature() {
		return getRemoteControllersAverageParameter(c->c.getLastSeenTemperrature());
	}

	public Double getRemoteControllersAverageHumidity() {
		return getRemoteControllersAverageParameter(c->c.getLastSeenHumidity());
	}

	public Double getRemoteControllersAveragePressure() {
		return getRemoteControllersAverageParameter(c->c.getLastSeenPressure());
	}

	public Double getRemoteControllersAverageParameter(Function<RemoteValvesController,Double> func) {
		double ret = Double.NaN;
		int count=0;
		for (RemoteValvesController controller:remoteControllersById.values()) {
			if (RemoteValvesControllerState.online.equals(controller.getState())) {
				ret = MathHelper.doubleSumIgnoreNaN(ret,func.apply(controller));
				count++;
			}
		}
		if (count>0 && !Double.isNaN(ret)) {
			return ret/count;
		} else {
			return Double.NaN;
		}
	}

	public void toggleProgramManual(String programId) {
		SchedulerProgram program = programs.get(programId);
		if (program!=null && program.isEnabled()) {
			Program activeProgram = getActiveProgram();
			if (activeProgram!=null) {
				activeProgram.stop();
			}
			program.start();
			gpioHandler.blink(1);
			setActiveProgram(program);
		}
	}

	public void toggleProgramEnable(String programId) {
		SchedulerProgram program = programs.get(programId);
		if (program!=null) {
			program.toggleEnable();
		}
	}

	public void resetController(String controllerId) {
		RemoteValvesController controller = remoteControllersById.get(controllerId);
		if (controller!=null && controller.isOnline()) {
			controller.sendResetRequest();
			LogHelper.log(logger, Level.INFO, "Reset controller: '%s'", controllerId);
		} else {
			LogHelper.log(logger, Level.WARNING, "Ignored reset controller: '%s'", controllerId);
		}
	}

	public SchedulerRunMode getRunMode() {
		return runMode;
	}

	public void toggleSchedulerState() {
		switch (runMode) {
		case running: runMode = SchedulerRunMode.suspended; break;
		case suspended: runMode = SchedulerRunMode.running; break;
		}
		LogHelper.log(logger, Level.INFO, "Toggle line scheduler mode: '%s'", runMode.name());
	}


}
