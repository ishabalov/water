package org.shabalov.water.scheduler;

@FunctionalInterface
public interface SchedulerCommand {
	void execute();
}
