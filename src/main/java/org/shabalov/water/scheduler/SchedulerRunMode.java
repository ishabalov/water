package org.shabalov.water.scheduler;

public enum SchedulerRunMode {
	running("RUN"),suspended("SUS");
	
	public final String label;
	private SchedulerRunMode(String label) {
		this.label = label;
	}
	protected static final SchedulerRunMode fromName(String name) {
		for (SchedulerRunMode m:values()) {
			if (name.equals(m.name())) {
				return m;
			}
		}
		return running;
	}
	
}
