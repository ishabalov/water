package org.shabalov.water.scheduler;

import java.time.LocalDateTime;
import java.util.logging.Logger;

public abstract class Program {
	
	public final Scheduler scheduler;
	public final String name;
	public final String id;
	protected final Logger logger;
	
	protected Program(Logger logger, Scheduler scheduler, String id, String name) {
		this.logger=logger;
		this.scheduler=scheduler;
		this.id=id;
		this.name=name;
	}
	
	protected abstract void start();
	public abstract ProgramState updateProgramState();
	
	protected abstract void stop();

	public abstract LocalDateTime getLastRunDateTime();

	public String getStatLabel() {
		return name;
	}
	
}
