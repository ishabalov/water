package org.shabalov.water.scheduler;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Month;

public class ProgramPeriod implements Externalizable {
	private Duration periodDuration;
	private long periodIndex;
	
	private static final LocalDateTime PERIOD_BASE = LocalDateTime.of(2000, Month.JANUARY, 1, 0, 0);
	protected ProgramPeriod(Duration periodDuration,LocalDateTime time) {
		assert periodDuration!=null;
		this.periodDuration=periodDuration;
		Duration fromPeriodBase = Duration.between(PERIOD_BASE,time);
		this.periodIndex= Math.floorDiv(fromPeriodBase.toMillis(),periodDuration.toMillis());
	}

	protected ProgramPeriod(Duration periodDuration) {
		this(periodDuration,LocalDateTime.now());
	}

	protected ProgramPeriod(ObjectInput in) throws ClassNotFoundException, IOException {
		readExternal(in);
	}
	
	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeLong(this.periodDuration.toMillis());
		out.writeLong(this.periodIndex);
		
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		this.periodDuration = Duration.ofMillis(in.readLong());
		this.periodIndex=in.readLong();
	}

	boolean isAfter(ProgramPeriod other) {
		if (this.periodDuration.equals(other.periodDuration)) {
			return this.periodIndex>other.periodIndex;
		} else {
			return true;
		}
	}

	public boolean hasSameDuration(Duration interval) {
		return periodDuration.equals(interval);
	}

	public ProgramPeriod translateToDuration(Duration interval) {
		LocalDateTime intervalStart = toIntervalStartLocalDateTime();
		return new ProgramPeriod(interval, intervalStart);
	}

	private LocalDateTime toIntervalStartLocalDateTime() {
		return PERIOD_BASE.plus(periodDuration.multipliedBy(periodIndex));
	}

	@Override
	public String toString() {
		return "ProgramPeriod [periodDuration=" + periodDuration + ", periodIndex=" + periodIndex + "]";
	}
	
	
	
//	public static final void main(String[] args) {
//		Duration d = Duration.ofMinutes(2);
//		LocalDateTime base1 = LocalDateTime.of(2020, 1, 1, 0, 0, 0, 0);
//		LocalDateTime base2= LocalDateTime.of(2020, 1, 1, 0, 1, 59, 0);
//		LocalDateTime base3= LocalDateTime.of(2020, 1, 1, 0, 2, 0, 0);
//		ProgramPeriod p1 = new ProgramPeriod(d,base1);
//		ProgramPeriod p2 = new ProgramPeriod(d,base2);
//		ProgramPeriod p3 = new ProgramPeriod(d,base3);
//		System.out.printf("%b%n", p2.isAfter(p1));
//		System.out.printf("%b%n", p3.isAfter(p1));
//	}
//	
}
