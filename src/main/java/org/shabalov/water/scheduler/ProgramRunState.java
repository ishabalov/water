package org.shabalov.water.scheduler;

public enum ProgramRunState {
	idle,running,suspended,disabled;
	
	public String format() {
		switch (this) {
		case idle: return "IDL";
		case running: return "RUN";
		case disabled : return "DIS";
		case suspended: return "SUS";
		default: return "DIS";
		}
	}

}
