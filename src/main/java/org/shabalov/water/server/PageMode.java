package org.shabalov.water.server;

public enum PageMode {
	lines {
		@Override
		public String label() {
			return "LNS";
		}
	},
	programs {
		@Override
		public String label() {
			return "PRG";
		}
	},
	controllers {
		@Override
		public String label() {
			return "CTL";
		}
	};


	public String label() {
		return null;
	}
}
