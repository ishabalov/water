package org.shabalov.water.server;

import java.util.logging.Logger;

import org.jbits.components.Component;

public interface StatProvider {
	String name();
	void collectStat();
	Component produceStat();
	void report(Logger logger);
}
