package org.shabalov.water.server;

import java.io.IOException;

import org.jbits.beans.BeansFactory;

public class Bootstrap {
	public static void main(String[] args) throws IOException, Exception {
		Server server = BeansFactory.getOrCreateBean(Server.class);
		server.startServer();
		server.waitForShutdown();
	}
}
