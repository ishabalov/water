package org.shabalov.water.server;

import java.time.Instant;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jbits.beans.Inject;
import org.shabalov.water.scheduler.Scheduler;

import com.google.gson.GsonBuilder;

@Path(WaterRestInterface.ROOT)
public class WaterRestInterface {

	@Inject
	private Scheduler scheduler;
	
	@Inject
	private RootHttpHandler rootHttpHandler;
	
	public static final String ROOT = "/";
	public static final String UPDATES = "updates";
	public static final String PING = "ping";
	public static final String STOP = "stop-server";
	public static final String LINE_MANUAL_TOGGLE = "line-toggle";
	public static final String LINE_MANUAL_TOGGLE_ENABLE = "line-toggle-enable";
	public static final String RUN_ALL_LINES_MANUAL = "run-all-lines-manual";
	public static final String TOGGLE_MODE = "toggle-mode";
	public static final String TOGGLE_PROGRAM = "toggle-program";
	public static final String TOGGLE_PROGRAM_ENABLE = "toggle-program-enable";
	public static final String RESET_CONTROLLER = "reset-controller";
	public static final String TOGGLE_SCHEDILER = "toggle-scheduler-state";

	/*
	 * All url skip root
	 */
	public String getServerActionUrl(String action) {
		return Server.API_BASE+action;
	}
	
	@GET
	@Path("/"+WaterRestInterface.PING)
	@Produces(MediaType.TEXT_PLAIN)
	public String ping() {
		return String.format("Hello, %s%n", Instant.now().toString());
	}

	@GET
	@Path("/"+WaterRestInterface.UPDATES)
	@Produces(MediaType.APPLICATION_JSON)
	public String getUpdates() {
		return toJson(rootHttpHandler.getBodyUpdates());
	}
	
	@POST
	@Path("/"+WaterRestInterface.STOP)
	@Produces(MediaType.TEXT_PLAIN)
	public String stopServer() {
		scheduler.requestExit();
		return "Stopping";
	}

	@POST
	@Path("/"+WaterRestInterface.LINE_MANUAL_TOGGLE)
	@Produces(MediaType.TEXT_HTML)
	public String lineToggle(@FormParam("lineId") final String lineId) {
		return doAction(()->scheduler.toggleLineManualSubmit(lineId));
	}

	@POST
	@Path("/"+WaterRestInterface.LINE_MANUAL_TOGGLE_ENABLE)
	@Produces(MediaType.TEXT_HTML)
	public String lineToggleEnable(@FormParam("lineId") final String lineId) {
		return doAction(()->scheduler.toggleEnableLineManualSubmit(lineId));
	}

	@POST
	@Path("/"+WaterRestInterface.RUN_ALL_LINES_MANUAL)
	@Produces(MediaType.TEXT_HTML)
	public String runAllLinesManual() {
		return doAction(()->scheduler.runAllLinesManualSubmit());
	}

	@POST
	@Path("/"+WaterRestInterface.TOGGLE_MODE)
	@Produces(MediaType.TEXT_HTML)
	public String toggleMode() {
		return doAction(()->rootHttpHandler.toggleMode());
	}

	@POST
	@Path("/"+WaterRestInterface.TOGGLE_PROGRAM)
	@Produces(MediaType.TEXT_HTML)
	public String toggleProgram(@FormParam("programId") final String programId) {
		return doAction(()->scheduler.toggleProgramManual(programId));
	}

	@POST
	@Path("/"+WaterRestInterface.TOGGLE_PROGRAM_ENABLE)
	@Produces(MediaType.TEXT_HTML)
	public String toggleProgramEnable(@FormParam("programId") final String programId) {
		return doAction(()->scheduler.toggleProgramEnable(programId));
	}

	@POST
	@Path("/"+WaterRestInterface.RESET_CONTROLLER)
	@Produces(MediaType.TEXT_HTML)
	public String resetController(@FormParam("controllerId") final String controllerId) {
		return doAction(()->scheduler.resetController(controllerId));
	}

	@POST
	@Path("/"+WaterRestInterface.TOGGLE_SCHEDILER)
	@Produces(MediaType.TEXT_HTML)
	public String toggleSchedulerState() {
		return doAction(()->scheduler.toggleSchedulerState());
	}
	
	private String doAction(Runnable action) {
		action.run();
		return rootHttpHandler.getBody();
	}
	
	public <T> String toJson(T value) {
		GsonBuilder builder = new GsonBuilder();
		return builder.create().toJson(value);
	}

	
}
