package org.shabalov.water.server;

import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jbits.beans.Inject;
import org.jbits.beans.PostConstruct;
import org.jbits.components.Component;
import org.jbits.components.Literal;
import org.jbits.log.LogHelper;
import org.shabalov.water.gpio.GpioHandler;

public class InputVoltageStatProvider implements StatProvider {
	
	@Inject
	private GpioHandler gpioHandler;
	
	private static final int BUFFER_LENGTH = 100;
	private static final double VOLTAGE_MULTIPLIER = 33.6;
	//private static final int AOI_CHANNEL = 0;
	
	private double[] circularBuffer = null;
	private int currentBufferPosition;
	private AtomicReference<Double> lastMeasure = new AtomicReference<Double>(Double.NaN);
	
	@PostConstruct
	private void init() {
		this.circularBuffer = new double[BUFFER_LENGTH];
		for (int i=0;i<BUFFER_LENGTH;i++) {
			circularBuffer[i]=Double.NaN;
		}
		currentBufferPosition=0;
	}

	@Override
	public Component produceStat() {
		Double ret = lastMeasure.get();
		if (ret!=null && ! ret.isNaN()) {
			return new Literal(String.format("%.03f V", ret));
		} else {
			return new Literal("--");
		}
	}

	@Override
	public void collectStat() {
//		float aioRead = gpioHandler.aioRead(AOI_CHANNEL);
		double voltage = 0.0*VOLTAGE_MULTIPLIER;
		circularBuffer[currentBufferPosition]=voltage;
		currentBufferPosition=++currentBufferPosition % BUFFER_LENGTH;
		double lastMeasure = Double.NaN;
		int counter=0;
		for (int i=0;i<BUFFER_LENGTH;i++) {
			if (!Double.isNaN(circularBuffer[i])) {
				counter++;
				if (!Double.isNaN(lastMeasure)) {
					lastMeasure+=circularBuffer[i];
				} else {
					lastMeasure=circularBuffer[i];
				}
			}
		}
		if (counter>0 && !Double.isNaN(lastMeasure)) {
			this.lastMeasure.set(lastMeasure/counter);
		} else {
			this.lastMeasure.set(Double.NaN);
		}
	}

	@Override
	public String name() {
		return "battery";
	}

	@Override
	public void report(Logger logger) {
		LogHelper.log(logger, Level.INFO, "%,f", lastMeasure.get()); 
	}

	
}
