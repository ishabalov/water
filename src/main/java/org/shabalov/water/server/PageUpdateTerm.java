package org.shabalov.water.server;

public enum PageUpdateTerm {
	innerHtml, inputValue, locationHref, evalExpression, setClass
}
