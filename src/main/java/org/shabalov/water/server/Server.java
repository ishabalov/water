package org.shabalov.water.server;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.glassfish.grizzly.http.server.CLStaticHttpHandler;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.jbits.beans.Inject;
import org.jbits.beans.PostConstruct;
import org.jbits.log.LogHelper;
import org.shabalov.water.config.ConfigHelper;
import org.shabalov.water.scheduler.Scheduler;
import org.shabalov.water.udp.UdpServer;

public class Server {

	public static final String DEFAULT_PROTOCOL="http";
	public static final String SERVER_NAME = "WATER";
	public static final String API_BASE = "/rest/";
	
	private HttpServer server = null;
	
	@Inject private WaterRestInterface restInterface;
	@Inject private UdpServer udpServer;
	@Inject private Logger logger;
	@Inject private Scheduler scheduler;
	@Inject private RootHttpHandler rootHandler;

	private CLStaticHttpHandler staticHandler;
	
	@PostConstruct
	protected void init() throws IOException {
		LogHelper.log(logger,Level.INFO,"Use root config folder: '%s'",ConfigHelper.getConfigRoot().toAbsolutePath().toString());
		this.staticHandler = new CLStaticHttpHandler(Thread.currentThread().getContextClassLoader(),"/static/");
		Files.createDirectories(ConfigHelper.getConfigRoot());
		Files.createDirectories(ConfigHelper.getDataRoot());
	}
	
	public void startServer() throws IOException {
		final ResourceConfig rc = new ResourceConfig()
				.register(restInterface);
		URI serverUri = URI.create(getRestUrl());
		LogHelper.log(logger,Level.INFO,"Server starting, URI:%s",getBaseUrl());
		this.server = GrizzlyHttpServerFactory.createHttpServer(serverUri, rc, false);
		this.server.getServerConfiguration().addHttpHandler(rootHandler,"/");
		this.server.getServerConfiguration().addHttpHandler(staticHandler,"/static/*");
		this.server.getServerConfiguration().setName(SERVER_NAME);
		this.server.start();
	}

	private String getBaseUrl() {
		return String.format("%s://%s:%d", 
				DEFAULT_PROTOCOL, 
				ConfigHelper.getBindingHostname(),
				ConfigHelper.getBindingPort()
			);
	}

	private String getRestUrl() {
		return getBaseUrl()+API_BASE; 
	}

	public void waitForShutdown() {
		scheduler.waitForExitRequest();
		server.shutdownNow();
	}

}
