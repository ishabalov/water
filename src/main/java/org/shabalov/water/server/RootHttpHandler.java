package org.shabalov.water.server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.Request;
import org.glassfish.grizzly.http.server.Response;
import org.glassfish.grizzly.http.util.Header;
import org.glassfish.grizzly.http.util.HttpStatus;
import org.glassfish.grizzly.http.util.MimeType;
import org.jbits.beans.Inject;
import org.jbits.beans.PostConstruct;
import org.jbits.collections.CollectionsHelper;
import org.jbits.components.Component;
import org.jbits.components.Div;
import org.jbits.components.Input;
import org.jbits.components.Literal;
import org.jbits.io.IoHelper;
import org.jbits.log.LogHelper;
import org.jbits.text.AtomicCachedValue;
import org.jbits.time.TimeIntervalFormat;
import org.shabalov.water.config.ConfigHelper;
import org.shabalov.water.scheduler.AbstractLine;
import org.shabalov.water.scheduler.LineStat;
import org.shabalov.water.scheduler.RemoteValvesController;
import org.shabalov.water.scheduler.Scheduler;
import org.shabalov.water.scheduler.SchedulerProgram;
import org.shabalov.water.scheduler.SchedulerProgramStat;

public class RootHttpHandler extends HttpHandler {

	@Inject
	private Logger logger;
	@Inject
	private Scheduler scheduler;

	//@Inject private InputVoltageStatProvider inputVoltageStatProvider;

	private Thread statTread = null;
	private List<StatProvider> statProviders = new ArrayList<>();
	private PageMode mode = PageMode.lines;

	private static final Duration STAT_COLLECT_INTERVAL = Duration.ofMillis(500);
	private static final Duration STAT_LOOP_INTERVAL = Duration.ofSeconds(4);
	private static final Duration STAT_REPORT_INTERVAL = Duration.ofSeconds(60);

	@PostConstruct
	private void init() {
		LogHelper.log(logger,  Level.INFO, "Init root http handler");
		statProviders.add(getRemoteControllersTemperratureStatProvider());
		statProviders.add(getRemoteControllersPressureStatProvider());
		statProviders.add(getRemoteControllersHumidityStatProvider());
		statTread = new Thread(getStatRunnable(), "Statistics Thread");
		statTread.setDaemon(false);
		statTread.start();
	}

	private StatProvider getRemoteControllersTemperratureStatProvider() {
		return new StatProvider() {

			private AtomicReference<Double> lastMeasure = new AtomicReference<Double>(Double.NaN);

			@Override
			public void report(Logger logger) {
				LogHelper.log(logger, Level.INFO, "%,.0fC", lastMeasure.get()); 
			}
			
			@Override
			public Component produceStat() {
				Double ret = lastMeasure.get();
				if (ret!=null && ! ret.isNaN()) {
					return new Literal(String.format("%.0fC", ret));
				} else {
					return new Literal("--");
				}
			}
			
			@Override
			public String name() {
				return "temperrature";
			}
			
			@Override
			public void collectStat() {
				lastMeasure.set(scheduler.getRemoteControllersAverageTemperrature());
			}
		};
	}

	private StatProvider getRemoteControllersPressureStatProvider() {
		return new StatProvider() {

			private AtomicReference<Double> lastMeasure = new AtomicReference<Double>(Double.NaN);

			@Override
			public void report(Logger logger) {
				LogHelper.log(logger, Level.INFO, "%,.0fhPa", lastMeasure.get()/100.0); 
			}
			
			@Override
			public Component produceStat() {
				Double ret = lastMeasure.get();
				if (ret!=null && ! ret.isNaN()) {
					return new Literal(String.format("%.0fhPa", ret/100.0));
				} else {
					return new Literal("--");
				}
			}
			
			@Override
			public String name() {
				return "pressure";
			}
			
			@Override
			public void collectStat() {
				lastMeasure.set(scheduler.getRemoteControllersAveragePressure());
			}
		};
	}

	private StatProvider getRemoteControllersHumidityStatProvider() {
		return new StatProvider() {

			private AtomicReference<Double> lastMeasure = new AtomicReference<Double>(Double.NaN);

			@Override
			public void report(Logger logger) {
				LogHelper.log(logger, Level.INFO, "%,.0f%%", lastMeasure.get()); 
			}
			
			@Override
			public Component produceStat() {
				Double ret = lastMeasure.get();
				if (ret!=null && ! ret.isNaN()) {
					return new Literal(String.format("%.0f%%", ret));
				} else {
					return new Literal("--");
				}
			}
			
			@Override
			public String name() {
				return "humidity";
			}
			
			@Override
			public void collectStat() {
				lastMeasure.set(scheduler.getRemoteControllersAverageHumidity());
			}
		};
	}

	private Runnable getStatRunnable() {
		return new Runnable() {

			@Override
			public void run() {
				Instant lastStatReported = null;
				while(!scheduler.isExitRequested()) {
					for (StatProvider provider:statProviders) {
						provider.collectStat();
					}
					if (lastStatReported==null || Duration.between(lastStatReported, Instant.now()).compareTo(STAT_REPORT_INTERVAL) > 0) {
						for (StatProvider provider:statProviders) {
							try (PrintWriter wr = new PrintWriter(Files.newOutputStream(ConfigHelper.getDataRoot().resolve(provider.name()+".log"), StandardOpenOption.APPEND, StandardOpenOption.CREATE))) {
								provider.report(LogHelper.getAnonymousLogger(wr));
							} catch (IOException e) {
								LogHelper.logException(logger, Level.WARNING, e, "Ignored exception while saving log for stat provider '%s'", provider.name());
							}
						}
						lastStatReported = Instant.now();
					}
					try {
						Thread.sleep(STAT_COLLECT_INTERVAL.toMillis());
					} catch (InterruptedException e) {
						// ignore
					}
				}
			}
		};
	}

	@Override
	public void service(Request request, Response response) throws Exception {
		String url = request.getRequestURI();
		if ("/".equals(url)) {
			response.setStatus(HttpStatus.OK_200);
			response.addDateHeader(Header.Date, System.currentTimeMillis());
			response.setContentType(MimeType.get("html"));
			sendContent(response, getContent());
		} else {
			response.setStatus(HttpStatus.NOT_FOUND_404);
			response.addDateHeader(Header.Date, System.currentTimeMillis());
			response.setContentType(MimeType.get("html"));
			sendContent(response, String.format("NOT FOUND: '%s'",url));
		}
	}


	private void sendContent(Response response, String content) throws IOException {
		// NO NIO stuff here.
		try (OutputStream os = response.getOutputStream()) {
			os.write(content.getBytes(StandardCharsets.UTF_8));
		}

	}
	private static final Pattern MACRO = Pattern.compile("\\{\\w+\\}",Pattern.DOTALL);
	private static final String TEMPLATE = "page-template.html";

	private AtomicCachedValue<String> rootTemplate = new AtomicCachedValue<>(() -> loadResource(TEMPLATE));

	private String getContent() throws IOException {
		String resource = rootTemplate.computeIfAbsent();
		// someday valid processing of macros
		return MACRO.matcher(resource).replaceAll(getBody());
	}

	public String getBody() {
		Div container = Div.newDiv("container");
		renderHeader(container);
		switch (mode) {
		default:
		case lines:
			renderBodyForLines(container);
			break;
		case programs:
			renderBodyForPrograms(container);
			break;
		case controllers:
			renderBodyForControllers(container);
			break;
		}
		return container.toString();
	}
	public PageUpdate[] getBodyUpdates() {
		List<PageUpdate> ret = new ArrayList<>();
		StringBuilder stat = new StringBuilder();
		getStatComponents().render(stat);
		ret.add(new PageUpdate(STAT_ID, stat.toString(), PageUpdateTerm.innerHtml));
		ret.add(new PageUpdate(TIMESTAMP_ID, getDateTimeString(), PageUpdateTerm.innerHtml));
		ret.add(new PageUpdate(PROGRAM_LAST_STAT, getLastProgramStat(), PageUpdateTerm.innerHtml));
		ret.add(new PageUpdate(PROGRAM_CURR_STAT, getActiveProgramStat(), PageUpdateTerm.innerHtml));
		ret.add(new PageUpdate(PROGRAM_NEXT_STAT, getNextProgramStat(), PageUpdateTerm.innerHtml));
		switch (mode) {
		case lines:
			bodyUpdatesForLines(ret);
			ret.add(new PageUpdate(BTN_RUN_ID, String.format("enable('%s')", BTN_RUN_ID), PageUpdateTerm.evalExpression));
			break;
		case programs:
			bodyUpdatesPrograms(ret);
			ret.add(new PageUpdate(BTN_RUN_ID, String.format("enable('%s')", BTN_RUN_ID), PageUpdateTerm.evalExpression));
			break;
		case controllers:
			bodyUpdatesControllers(ret);
			ret.add(new PageUpdate(BTN_RUN_ID, String.format("disable('%s')", BTN_RUN_ID), PageUpdateTerm.evalExpression));
			break;
		default:
			break;
		}
		return CollectionsHelper.asArray(PageUpdate.class, ret);
	}	
	

	public void toggleMode() {
		int nextModeIndex = (this.mode.ordinal()+1)%PageMode.values().length;
		this.mode = PageMode.values()[nextModeIndex];
	}
	
	public void renderBodyForLines(Div container) {
		boolean even=false;
		for (AbstractLine line:scheduler.getSortedLines()) {
			renderLine(container,line,even);
			even=!even;
		}
	}

	public void renderBodyForPrograms(Div container) {
		boolean even=false;
		for (SchedulerProgram program:scheduler.getSortedPrograms()) {
			renderProgram(container,program,even);
			even=!even;
		}
	}

	public void renderBodyForControllers(Div container) {
		boolean even=false;
		for (RemoteValvesController controller:scheduler.getSortedControllers()) {
			renderController(container,controller,even);
			even=!even;
		}
	}
	
	private static final String STAT_ID = "stat";
	private static final String TIMESTAMP_ID = "timestamp";
	private static final String BTN_MODE_ID = "btn_mode";
	private static final String BTN_RUN_ID = "btn_run";
	private static final String BTN_SCHED_ID = "btn_sched";
	private static final String BTN_1_PREFIX = "btn_1_";
	private static final String BTN_2_PREFIX = "btn_2_";
	private static final String BTN_3_PREFIX = "btn_3_";
	private static final String PROGRAM_LAST_STAT = "program_stat_last_v";
	private static final String PROGRAM_CURR_STAT = "program_stat_curr_v";
	private static final String PROGRAM_NEXT_STAT = "program_stat_next_v";
	private static final String STAT_PREFIX = "stat_";
	private static final String NAME_PREFIX = "name_";
	
	private static final String STAT_CLASS = "stat";
	private static final String NAME_CLASS = "name";
	private static final String BTN_CLASS = "std_btn";
	
	private static final String PRG_LABEL_CLASS = "program_label";
	private static final String PRG_CURRENT_CLASS = "program_current";
	private static final String PRG_VALUE_CLASS = "program_value";


	private static final String getButtonId1(String id) {
		return BTN_1_PREFIX+id;
	}
	private static final String getButtonId2(String id) {
		return BTN_2_PREFIX+id;
	}
	private static final String getButtonId3(String id) {
		return BTN_3_PREFIX+id;
	}
	private static final String getStatId(String id) {
		return STAT_PREFIX+id;
	}
	private static final String getNameId(String id) {
		return NAME_PREFIX+id;
	}


	private String getLineButtonValue(AbstractLine line) {
		return line.getLineState().format();
	}

	private String getLineEnableButtonValue(AbstractLine line) {
		return line.getActivityState().format();
	}

	private String getProgramButtonValue(SchedulerProgram program) {
		return program.getProgramRunState().format();
	}
	private String getProgramEnableButtonValue(SchedulerProgram program) {
		return program.getProgramEnabledState().format();
	}

	private String getControllerButtonValue(RemoteValvesController controller) {
		return controller.getState().format();
	}
	private String getControllerEnableButtonValue(RemoteValvesController controller) {
		return controller.getEnabledState().format();
	}
	
	private void bodyUpdates(List<PageUpdate> updates, String id, String button1Value, boolean button1Enabled, String button2Value, boolean button2Enabled, Component stat, boolean even, boolean isOn) {
		String nameDivId = getNameId(id);
		String statDivId = getStatId(id);
		String extraClass = extraClass(even, isOn);
		if (extraClass!=null) {
			updates.add(new PageUpdate(nameDivId, NAME_CLASS+" "+extraClass, PageUpdateTerm.setClass));
			updates.add(new PageUpdate(statDivId, STAT_CLASS+" "+extraClass, PageUpdateTerm.setClass));
		} else {
			updates.add(new PageUpdate(nameDivId, NAME_CLASS, PageUpdateTerm.setClass));
			updates.add(new PageUpdate(statDivId, STAT_CLASS, PageUpdateTerm.setClass));
		}
		String btnId1 = getButtonId1(id);
		updates.add(new PageUpdate(btnId1, button1Value, PageUpdateTerm.inputValue));
		if (button1Enabled) {
			updates.add(new PageUpdate(btnId1, String.format("enable('%s')", btnId1), PageUpdateTerm.evalExpression));
		} else {
			updates.add(new PageUpdate(btnId1, String.format("disable('%s')", btnId1), PageUpdateTerm.evalExpression));
		}
		String btnId2 = getButtonId2(id);
		updates.add(new PageUpdate(btnId2, button2Value, PageUpdateTerm.inputValue));
		if (button2Enabled) {
			updates.add(new PageUpdate(btnId2, String.format("enable('%s')", btnId2), PageUpdateTerm.evalExpression));
		} else {
			updates.add(new PageUpdate(btnId2, String.format("disable('%s')", btnId2), PageUpdateTerm.evalExpression));
		}
		updates.add(new PageUpdate(getStatId(id), stat.toString(), PageUpdateTerm.innerHtml));
	}

	private void bodyUpdatesForLines(List<PageUpdate> updates) {
		boolean even=false;
		for (AbstractLine line:scheduler.getSortedLines()) {
			bodyUpdates(
					updates, 
					line.id, 
					getLineButtonValue(line), 
					line.isEnabled(),
					getLineEnableButtonValue(line),
					line.isActive(),
					getLineStatComponents(line),
					even,
					line.isOn()
			);
			even=!even;
		}
	}

	private void bodyUpdatesPrograms(List<PageUpdate> updates) {
		boolean even=false;
		for (SchedulerProgram program:scheduler.getSortedPrograms()) {
			bodyUpdates(
					updates, 
					program.id, 
					getProgramButtonValue(program), 
					program.isEnabled(),
					getProgramEnableButtonValue(program),
					program.isActive(),
					getProgramStatComponents(program),
					even,
					program.isRunning()
			);
			even=!even;
		}
	}

	private void bodyUpdatesControllers(List<PageUpdate> updates) {
		boolean even=false;
		for (RemoteValvesController controller:scheduler.getSortedControllers()) {
			bodyUpdates(
					updates, 
					controller.id, 
					getControllerButtonValue(controller), 
					controller.isEnabled(),
					getControllerEnableButtonValue(controller),
					controller.isActive,
					getControllerStatComponents(controller),
					even,
					false
			);
			even=!even;
		}
	}

	
	private void renderHeader(Div container) {
		Div stat = Div.newDiv("head_stat")
				.with(
						Div.newLiteralDiv(TIMESTAMP_ID, getDateTimeString()),
						Div.newDiv(STAT_ID).with(
								getStatComponents()
						)
				);
		Div programs = Div.newDiv("program_stat")
				.with(
						Div.newLiteralDiv("Last:").withClasses(PRG_LABEL_CLASS),
						Div.newLiteralDiv(PROGRAM_LAST_STAT,getLastProgramStat()).withClasses(PRG_VALUE_CLASS),
						Div.newLiteralDiv("Active:").withClasses(PRG_LABEL_CLASS, PRG_CURRENT_CLASS),
						Div.newLiteralDiv(PROGRAM_CURR_STAT,getActiveProgramStat()).withClasses(PRG_VALUE_CLASS,PRG_CURRENT_CLASS),
						Div.newLiteralDiv("Next:").withClasses(PRG_LABEL_CLASS),
						Div.newLiteralDiv(PROGRAM_NEXT_STAT,getNextProgramStat()).withClasses(PRG_VALUE_CLASS)
					); 
		Input<String> button1 = Input.newButton(getRunValue())
				.withId(BTN_RUN_ID)
				.withClasses(BTN_CLASS)
				.withOnClick(getRunButtonOnClick());
		Input<String> button2 = Input.newButton(getSchedulerStateValue())
				.withId(BTN_SCHED_ID)
				.withClasses(BTN_CLASS)
				.withOnClick(getSchedulerSuspendResumeOnClick());
		Input<String> button3 = Input.newButton(getModeValue())
				.withId(BTN_MODE_ID)
				.withClasses(BTN_CLASS)
				.withOnClick("toggleMode();");
		switch (mode) {
		case lines:
		case programs:
			button1.setDisabled(false);
			break;
		case controllers:
		default:
			button1.setDisabled(true);
		}
		container.add(stat,programs,button1,button2,button3);
	}

	private String getRunButtonOnClick() {
		switch (mode) {
		case lines:
			return "toggleRunAllLinesManual();";
		case programs:
			return "toggleSchedulerState();";
		case controllers:
		default:
			return null;
		}
	}

	private String getSchedulerSuspendResumeOnClick() {
		return "toggleSchedulerSuspendResume();";
	}
	private String getSchedulerStateValue() {
		switch (scheduler.getRunMode()) {
		case running: return "SUS";
		case suspended: return "RES";
		default: return null;
		}
	}

	private String getLastProgramStat() {
		return getProgramStatString(scheduler.getLastProgramStat());
	}
	private String getActiveProgramStat() {
		return getProgramStatString(scheduler.getActiveProgramStat());
	}
	private String getNextProgramStat() {
		return getProgramStatString(scheduler.getNextProgramStat());
	}
	private String getProgramStatString(SchedulerProgramStat stat) {
		if (stat!=null) {
			StringBuilder ret = new StringBuilder();
			ret.append('\'');
			ret.append(stat.name);
			ret.append('\'');
			if (stat.timestamp!=null) {
				ret.append(" - ");
				ret.append(stat.timestamp.format(DATE_TIME_FORMATTER));
			}
			return ret.toString();
		} else {
			return "";
		}
	}
	private String getModeValue() {
		return mode.label();
	}
	private String getRunValue() {
		return scheduler.getRunMode().label;
	}

	private int currentStat = 0;
	private Instant lastStatChange = null;
	private Component getStatComponents() {
		if (statProviders.size()>0) {
			if (needStatChange()) {
				currentStat = (currentStat+1) % statProviders.size();
				lastStatChange = Instant.now();
			}
			if (currentStat<statProviders.size()) {
				return statProviders.get(currentStat).produceStat();
			} else {
				return Literal.EMPTY_COMPONENT;
			}
		} else {
			return Literal.EMPTY_COMPONENT;
		}
	}

	private boolean needStatChange() {
		return lastStatChange==null || lastStatChange.plus(STAT_LOOP_INTERVAL).isBefore(Instant.now());
	}

	private static final String extraClass(boolean even, boolean on) {
		if (on) {
			return "on";
		} else {
			return even?"even":null;
		}
	}
	
	private void renderRow(
			Div container, 
			String id, 
			String name, 
			Component stat, 
			String button1Value, 
			String button1Function,
			boolean button1Enabled,
			String button2Value, 
			String button2Function,
			boolean button2Enabled,
			String button3Value, 
			String button3Function,
			boolean button3Enabled,
			boolean even,
			boolean isOn) {
		String button1OnClickStr = String.format("%s('%s');", button1Function,id);
		// Name
		Div nameDiv = Div.newLiteralDiv(getNameId(id),name)
				.withClasses(NAME_CLASS,extraClass(even,isOn))
				.withOnClick(button1OnClickStr);
		// Line stat
		Div statDiv = Div.newDiv(getStatId(id))
				.withClasses(STAT_CLASS,extraClass(even, isOn))
				.withOnClick(button1OnClickStr)
				.with(stat);
		// Button 1
		Input<String> button1 = Input.newButton(button1Value)
				.withId(getButtonId1(id))
				.withClasses(BTN_CLASS)
				.withOnClick(button1OnClickStr);
		button1.setDisabled(!button1Enabled);
		// Button 2
		Input<String> button2 = Input.newButton(button2Value)
				.withId(getButtonId2(id))
				.withClasses(BTN_CLASS)
				.withOnClick(String.format("%s('%s');", button2Function,id));
		// Button 3 
		Input<String> button3 = Input.newButton(button3Value)
				.withId(getButtonId3(id))
				.withClasses(BTN_CLASS)
				.withOnClick(button3Function!=null?String.format("%s('%s');", button3Function,id):null);
		button2.setDisabled(!button3Enabled);
		container.add(nameDiv,statDiv,button1,button2,button3);
	}
	
	private void renderLine(Div container,AbstractLine line, boolean even) {
		renderRow(
				container, 
				line.id, 
				line.name, 
				getLineStatComponents(line), 
				getLineButtonValue(line), 
				"toggleLineManual", 
				line.isEnabled(),
				getLineEnableButtonValue(line),
				"toggleEnableLineManual", 
				line.isActive(),
				null,
				null, 
				line.isActive(),
				even,
				line.isOn());
	}

	
	private void renderProgram(Div container, SchedulerProgram program,boolean even) {
		renderRow(
				container,
				program.id,
				program.name,
				getProgramStatComponents(program),
				getProgramButtonValue(program),
				"toggleProgramManual", 
				program.isEnabled(),
				getProgramEnableButtonValue(program),
				"toggleEnableProgramManual", 
				program.isActive(),
				null,
				null, 
				program.isActive(),
				even,
				program.isRunning());
	}

	private void renderController(Div container, RemoteValvesController controller, boolean even) {
		renderRow(
				container,
				controller.id,
				controller.name,
				getControllerStatComponents(controller),
				getControllerButtonValue(controller),
				"resetController", 
				controller.isEnabled(),
				getControllerEnableButtonValue(controller),
				"toggleEnableControllerManual", 
				controller.isActive,
				"RES",
				"resetController", 
				controller.isActive,
				even,
				false);
	}
	
	private static final String formatDuration(Duration duration) {
		int hours = (int)duration.toHoursPart();
		int minutes = (int)duration.toMinutesPart();
		int seconds = (int)duration.toSecondsPart();
		List<String> parts = new ArrayList<>();
		if (hours>0) {
			parts.add(String.format("%dh", hours));
		}
		if (minutes>0 || hours>0) {
			parts.add(String.format("%dm", minutes));
		}
		parts.add(String.format("%ds", seconds));
		return parts.stream().collect(Collectors.joining(" "));
	}
	
	private Component getLineStatComponents(AbstractLine line) {
		LineStat stat = line.getLineStat();
		if (stat!=null) {
			StringBuilder ret = new StringBuilder();
			if (stat.lastOn!=null && stat.lastOn.getYear()>1970) {
				ret.append(String.format("%s", stat.lastOn.format(DATE_TIME_FORMATTER)));
			}
			if (stat.lastOnDuration!=null && stat.lastOnDuration.toMillis()>0) {
				ret.append(String.format(" %s", formatDuration(stat.lastOnDuration)));
			}
			return new Literal(ret.toString());
		} else {
			return Component.EMPTY_COMPONENT;
		}
	}
	public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("MMM-dd HH:mm:ss");

	private String getDateTimeString() {
		return LocalDateTime.now().format(DATE_TIME_FORMATTER) ;
	}
	
	private Component getProgramStatComponents(SchedulerProgram program) {
			StringBuilder ret = new StringBuilder();
			LocalDateTime lastStart = program.getLastRunDateTime(); 
			if (lastStart !=null && lastStart.getYear()>1970) {
				ret.append(String.format("%s", lastStart.format(DATE_TIME_FORMATTER)));
			}
			return new Literal(ret.toString());
	}

	private Component getControllerStatComponents(RemoteValvesController controller) {
		StringBuilder ret = new StringBuilder();
		if (controller.isOnline()) {
			Duration uptime = controller.getLastSeenUptime();
			
			ret.append(
				String.format(
						"%s %.1fC %.1fhPa %.1f%%", 
						TimeIntervalFormat.formatMedium(uptime),
						controller.getLastSeenTemperrature(),
						controller.getLastSeenPressure()/100.0,
						controller.getLastSeenHumidity()
					)
				);
		}
		return new Literal(ret.toString());
}

	protected static final String loadResource(String path) {
		try (InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream(path)) {
			return IoHelper.loadStreamToString(in);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}


	public static final void main(String[] str) {
		Duration d = Duration.ofMillis(101101);
		System.out.println(TimeIntervalFormat.formatShort(d));

	}
}
