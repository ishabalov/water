package org.shabalov.water.server;

public class PageUpdate {
	public final String id;
	public final String text;
	public final PageUpdateTerm term;
	protected PageUpdate(String id, String text, PageUpdateTerm term) {
		this.id = id;
		this.text = text;
		this.term = term;
	}
}
