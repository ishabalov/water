#!/bin/bash
TARGET_PLATFORM='pi4dev'
TARGET_HOME='/home/pi'
TARGET_USER='pi'

JAVA_HOME=/scratch/tools/java

echo "#!/bin/bash
export WATER_HOSTNAME=0.0.0.0
export WATER_PORT=8011
export WATER_ROOT=/home/pi/water
export WATER_DIST=/home/pi/water
export JAVA_HOME=/scratch/tools/java
export JAVA_OPTS='-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=0.0.0.0:8040'
tar xvzf -
. water/start.sh" | ssh $TARGET_USER@$TARGET_PLATFORM "cat - >$TARGET_HOME/boot.sh"


cat target/water-0.0.1-SNAPSHOT-build.tar.gz | ssh $TARGET_USER@$TARGET_PLATFORM bash $TARGET_HOME/boot.sh
