#!/bin/bash
# setting environment, please replace with specific parameters
export WATER_HOSTNAME=10.1.10.247
export WATER_PORT=8011
export WATER_ROOT=/home/pi/water-home
export WATER_DIST=/home/pi/water-dist/water
export JAVA_HOME=/scratch/tools/jdk-11.0.10+9
export PATH=$PATH:$JAVA_HOME/bin
#
export JAVA_OPTS="-Dcom.sun.management.jmxremote -Djava.rmi.server.hostname=water -Dcom.sun.management.jmxremote.port=1219 -Dcom.sun.management.jmxremote.rmi.port=1219 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false"
#
# File location /home/pi/water-dist/water
CP=""
for f in $WATER_DIST/lib/*.jar
do
        CP=$CP:$f
done

$JAVA_HOME/bin/java $JAVA_OPTS --class-path $CP org.shabalov.water.server.Bootstrap
